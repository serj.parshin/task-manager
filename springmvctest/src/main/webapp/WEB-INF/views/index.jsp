<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <%@include file="parts/head.jsp" %>
    <title>Index</title>
</head>
<body>
	<div class="container">
		<%@include file="parts/nav-bar.jsp" %>
    	<p>
    	    hashCode: ${hashCode}
    	</p>
    	<%@include file="parts/footer.jsp" %>
	</div>
</body>
</html>