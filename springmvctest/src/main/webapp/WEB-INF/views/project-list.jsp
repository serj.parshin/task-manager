<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <%@include file="parts/head.jsp" %>
    <title>Project List</title>
</head>
<body>
	<div class="container">
		<%@include file="parts/nav-bar.jsp" %>
    	<table class="table table-hover">
    		<thead class="thead-light">
    			<tr>
    				<th scope="col">#</th>
    				<th scope="col">ID</th>
    				<th scope="col">NAME</th>
    				<th scope="col">DESCRIPTION</th>
    				<th scope="col" colspan="2">ACTIONS</th>
    			</tr>
    		</thead>
    		<tbody>
    			<c:forEach var="project" items="${projectList}" varStatus="loop">
    				<tr>
    					<td scope="row">${loop.index +1}</td>
    					<td>${project.id}</td>
    					<td>${project.name}</td>
    					<td>${project.description}</td>
    					<td><a href="<c:url value='/project-edit/${project.id}'/>">EDIT</a></td>
    					<td><a href="<c:url value='/project-delete/${project.id}'/>">DELETE</a></td>
    				</tr>
    			</c:forEach>
    		</tbody>
    	</table>
	</div>
    <%@include file="parts/footer.jsp" %>
</body>
</html>