<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
	<%@include file="parts/head.jsp" %>
	<title>Project List</title>
</head>
<body>
	<div class="container">
		<%@include file="parts/nav-bar.jsp" %>
		<form:form action="/project-edit/${project.id}" method="post" modelAttribute="project">
		    <form:input type="hidden" class="form-control" id="projectId" path="id"/>
			<div class="form-group">
				<form:label for="projectName" path="name">Name</form:label>
				<form:input type="text" class="form-control" id="projectName" path="name"/>
			</div>
			<div class="form-group">
				<form:label for="projectDescription" path="description">Description</form:label>
				<form:textarea class="form-control" id="projectDescription" rows="3" path="description"/>
			</div>
			<div class="form-group">
            	<form:label for="projectDateBegin" path="dateBegin">Date begin</form:label>
            	<form:input type="datetime-local" class="form-control" id="projectDateBegin" path="dateBegin"/>
            </div>
            <div class="form-group">
            	<form:label for="projectDateEnd" path="dateEnd">Date end</form:label>
            	<form:input type="datetime-local" class="form-control" id="projectDateEnd" path="dateEnd"/>
            </div>
			<div class="form-group">
				<form:label for="projectStage" path="stage">Stage</form:label>
				<form:select class="custom-select" id="projectStage" path="stage">
						<form:options items="${stages}" itemLabel="displayName"/>
				</form:select>
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
			<a href="<c:url value='/project-list'/>" class="btn btn-light" role="button">Back</a>
		</form:form>
	</div>
	<%@include file="parts/footer.jsp" %>
</body>
</html>