<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">Main</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/project-list">Projects</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/task-list">Tasks</a>
      </li>
    </ul>
  </div>
</nav>