package ru.parshin.test.mvc.api.repository;


import ru.parshin.test.mvc.domain.embedded.Stage;
import ru.parshin.test.mvc.domain.entity.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Component
public class DefaultDataInitializer {

    @Autowired
    private ProjectRepository projectRepository;

    @PostConstruct
    private void initDefaultData() {

        Project projectA = new Project();
        projectA.setName("Project A");
        projectA.setDescription("Description of A project");
        projectA.setDateBegin(LocalDateTime.now());
        projectA.setDateEnd(LocalDateTime.now());
        projectA.setStage(Stage.SCHEDULED);

        Project projectB = new Project();
        projectB.setName("Project B");
        projectB.setDescription("Description of B project");
        projectB.setDateBegin(LocalDateTime.now());
        projectB.setDateEnd(LocalDateTime.now());
        projectB.setStage(Stage.SCHEDULED);

        projectRepository.save(projectA);
        projectRepository.save(projectB);
    }
}
