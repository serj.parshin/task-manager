package ru.parshin.test.mvc.domain.embedded;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum Stage implements Serializable {

    SCHEDULED("Запланировано"),
    PROCESSING("В процессе"),
    SUCCESS("Готово");

    @Nullable
    private String displayName;
}
