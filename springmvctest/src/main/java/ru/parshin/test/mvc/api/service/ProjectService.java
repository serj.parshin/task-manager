package ru.parshin.test.mvc.api.service;

import ru.parshin.test.mvc.domain.entity.Project;

import java.util.Optional;

public interface ProjectService {

    Project save(Project project);

    Iterable<Project> saveAll(Iterable<Project> projects);

    Optional<Project> findById(String id);

    Iterable<Project> findAll();

    Iterable<Project> findAllById(String id);

    void deleteById(String id);

    void delete(Project project);

    void deleteAll(Iterable<Project> projects);
}
