package ru.parshin.test.mvc.domain.entity;

import org.springframework.format.annotation.DateTimeFormat;
import ru.parshin.test.mvc.domain.embedded.Stage;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Getter
@Setter
@EqualsAndHashCode(of = "id", callSuper = true)
public class Project extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Stage stage;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateBegin;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateEnd;
}
