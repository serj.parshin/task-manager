package ru.parshin.test.mvc.repository;

import ru.parshin.test.mvc.api.repository.ProjectRepository;
import ru.parshin.test.mvc.domain.entity.Project;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ProjectRepositoryImpl implements ProjectRepository {

    private final Map<String, Project> repository = new HashMap<>();

    @Override
    public Project save(Project project) {
        return repository.put(project.getId(), project);
    }

    @Override
    public Iterable<Project> saveAll(Iterable<Project> projects) {
        projects.forEach(project -> repository.put(project.getId(), project));
        return projects;
    }

    @Override
    public Optional<Project> findById(String id) {
        return Optional.ofNullable(repository.get(id));
    }

    @Override
    public Iterable<Project> findAll() {
        return repository.values();
    }

    @Override
    public Iterable<Project> findAllById(String ids) {
        return repository.values().stream()
                .filter(project -> project.getId().equals(ids))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(String id) {
        repository.remove(id);
    }

    @Override
    public void delete(Project project) {
        repository.remove(project.getId(), project);
    }

    @Override
    public void deleteAll(Iterable<Project> projects) {
        projects.forEach(project -> repository.remove(project.getId()));
    }
}
