package ru.parshin.test.mvc.service;

import ru.parshin.test.mvc.api.repository.ProjectRepository;
import ru.parshin.test.mvc.api.service.ProjectService;
import ru.parshin.test.mvc.domain.entity.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository repository;

    public Project save(Project project) {
        return repository.save(project);
    }

    public Iterable<Project> saveAll(Iterable<Project> projects) {
        return repository.saveAll(projects);
    }

    public Optional<Project> findById(String id) {
        return repository.findById(id);
    }

    public Iterable<Project> findAll() {
        return repository.findAll();
    }

    public Iterable<Project> findAllById(String id) {
        return repository.findAllById(id);
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }

    public void delete(Project project) {
        repository.delete(project);
    }

    public void deleteAll(Iterable<Project> projects) {
        repository.deleteAll(projects);
    }
}
