package ru.parshin.test.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class IndexController {

    @RequestMapping("/")
    public String index(Model model, HttpSession httpSession) {

        model.addAttribute("hashCode", hashCode());
        return "index";
    }
}
