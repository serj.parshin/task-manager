package ru.parshin.test.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.parshin.test.mvc.api.service.ProjectService;
import ru.parshin.test.mvc.domain.entity.Project;

import javax.servlet.http.HttpSession;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/project-list", method = RequestMethod.GET)
    public String projectList(Model model, HttpSession httpSession) {
        model.addAttribute("projectList", projectService.findAll());
        return "project-list";
    }

    @RequestMapping(value = "/project-edit/{id}", method = RequestMethod.GET)
    public String projectView(@PathVariable("id") String id, Model model, HttpSession httpSession) {
        model.addAttribute("project", projectService.findById(id).orElseThrow(IllegalArgumentException::new));
        return "project";
    }

    @RequestMapping(value = "/project-edit/{id}", method = RequestMethod.POST)
    public String projectSave(@ModelAttribute("project") Project project) {
        projectService.save(project);
        return "project";
    }
}
