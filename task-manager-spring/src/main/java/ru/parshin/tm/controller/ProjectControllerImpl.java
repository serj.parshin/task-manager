package ru.parshin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.parshin.tm.api.rest.ProjectController;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.exception.EmptyDTOException;
import ru.parshin.tm.exception.ProjectNotFoundException;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/project")
public class ProjectControllerImpl implements ProjectController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;

    public ProjectDTO get(@PathVariable("id") String id) {
        try {
            return projectService.findById(UUID.fromString(id)).orElseThrow(ProjectNotFoundException::new).toDTO();
        } catch (ProjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Project Not Found", e);
        }
    }

    public Iterable<ProjectDTO> getAll() {
        try {
            return StreamSupport
                    .stream(projectService.findAll().spliterator(), false)
                    .map(Project::toDTO)
                    .collect(Collectors.toList());
        } catch (ProjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Project Not Found", e);
        }
    }

    public ProjectDTO post(@RequestBody ProjectDTO projectDTO) {
        try {
            return projectService.save(fromDTO(projectDTO)).toDTO();
        } catch (EmptyDTOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Empty DTO", e);
        }
    }

    public void put(@RequestBody ProjectDTO projectDTO) {
        try {
            projectService.save(fromDTO(projectDTO));
        } catch (EmptyDTOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Empty DTO", e);
        }
    }

    public void delete(@PathVariable("id") String id) {
        projectService.deleteById(UUID.fromString(id));
    }

    @NotNull
    private Project fromDTO(@Nullable final ProjectDTO dto) {

        if (dto == null) throw new EmptyDTOException();

        final Project project = new Project();

        if (projectService.existsById(dto.getId())) project.setId(dto.getId());
        project.setDateCreate(dto.getDateCreate());
        project.setName(dto.getName());
        project.setDescription(dto.getDescription());
        project.setDateBegin(dto.getDateBegin());
        project.setDateEnd(dto.getDateEnd());
        project.setStage(dto.getStage());

        return project;
    }
}
