package ru.parshin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import ru.parshin.tm.api.rest.TaskController;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.domain.dto.TaskDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.Task;
import ru.parshin.tm.exception.EmptyDTOException;
import ru.parshin.tm.exception.ProjectNotFoundException;
import ru.parshin.tm.exception.TaskNotFoundException;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/task")
public class TaskControllerImpl implements TaskController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;

    public TaskDTO get(@PathVariable("id") String id) {
        try {
            return taskService.findById(UUID.fromString(id)).orElseThrow(TaskNotFoundException::new).toDTO();
        } catch (TaskNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task Not Found", e);
        }
    }

    public Iterable<TaskDTO> getAll() {
        try {
            return StreamSupport
                    .stream(taskService.findAll().spliterator(), false)
                    .map(Task::toDTO)
                    .collect(Collectors.toList());
        } catch (TaskNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task Not Found", e);
        }
    }

    public TaskDTO post(@RequestBody TaskDTO taskDTO) {
        try {
            return taskService.save(fromDTO(taskDTO)).toDTO();
        } catch (EmptyDTOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Empty DTO", e);
        }
    }

    public void put(@RequestBody TaskDTO taskDTO) {
        try {
            taskService.save(fromDTO(taskDTO));
        } catch (EmptyDTOException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Empty DTO", e);
        }
    }

    public void delete(@PathVariable("id") String id) {
        taskService.deleteById(UUID.fromString(id));
    }

    @NotNull
    private Task fromDTO(@Nullable final TaskDTO dto) {

        if (dto == null) throw new EmptyDTOException();

        final Task task = new Task();

        if (dto.getProjectId() != null) {
            final Project project = projectService
                    .findById(dto.getProjectId())
                    .orElseThrow(ProjectNotFoundException::new);
            task.setProject(project);
        }

        if (taskService.existsById(dto.getId())) task.setId(dto.getId());
        task.setDateCreate(dto.getDateCreate());
        task.setName(dto.getName());
        task.setDescription(dto.getDescription());
        task.setDateBegin(dto.getDateBegin());
        task.setDateEnd(dto.getDateEnd());
        task.setStage(dto.getStage());

        return task;
    }
}
