package ru.parshin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

    @WebMethod
    Task createTask(
            @WebParam(name = "projectId")
            @Nullable final String projectId,
            @WebParam(name = "name")
            @Nullable final String name);

    @WebMethod
    Task readTask(
            @WebParam(name = "projectId")
            @Nullable final String projectId,
            @WebParam(name = "taskId")
            @Nullable final String taskId);

    @WebMethod
    List<Task> readAllTasks(
            @WebParam(name = "projectId")
            @Nullable final String projectId,
            @WebParam(name = "orderBy")
            @Nullable final String orderBy);

    @WebMethod
    Task updateTask(
            @WebParam(name = "task")
            @Nullable final Task task);

    @WebMethod
    void deleteTask(
            @WebParam(name = "task")
            @Nullable final Task task);
}
