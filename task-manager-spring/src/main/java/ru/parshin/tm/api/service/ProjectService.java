package ru.parshin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.entity.Project;

import java.util.Optional;
import java.util.UUID;

public interface ProjectService {

    @Nullable Project save(@NotNull final Project entity);

    @NotNull Optional<Project> findById(@NotNull final UUID uuid);

    @Nullable Iterable<Project> saveAll(@NotNull final Iterable<Project> entities);

    @Nullable Iterable<Project> findAllById(@NotNull final Iterable<UUID> uuids);

    @Nullable Iterable<Project> findAll();

    boolean existsById(@NotNull final UUID uuid);

    boolean countById(@NotNull final UUID uuid);

    long count();

    void deleteById(@NotNull final UUID uuid);

    void delete(@NotNull final Project entity);

    void deleteAll(@NotNull final Iterable<? extends Project> entities);

    void deleteAll();
}
