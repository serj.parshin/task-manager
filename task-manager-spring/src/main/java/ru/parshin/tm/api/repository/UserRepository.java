package ru.parshin.tm.api.repository;

import org.springframework.data.repository.CrudRepository;
import ru.parshin.tm.domain.entity.User;

import java.util.UUID;

public interface UserRepository extends CrudRepository<User, UUID> {

    User findByLogin(final String userLogin);

    boolean countById(UUID id);
}
