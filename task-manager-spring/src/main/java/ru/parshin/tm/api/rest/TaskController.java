package ru.parshin.tm.api.rest;

import org.springframework.web.bind.annotation.*;
import ru.parshin.tm.domain.dto.TaskDTO;

public interface TaskController {

    @GetMapping(value = "/{id}", produces = "application/json")
    TaskDTO get(@PathVariable("id") String id);

    @GetMapping(value = "/all", produces = "application/json")
    Iterable<TaskDTO> getAll();

    @PostMapping(produces = "application/json", consumes = "application/json")
    TaskDTO post(@RequestBody TaskDTO taskDTO);

    @PutMapping(produces = "application/json", consumes = "application/json")
    void put(@RequestBody TaskDTO taskDTO);

    @DeleteMapping(value = "/{id}")
    void delete(@PathVariable("id") String id);
}
