package ru.parshin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.entity.Task;

import java.util.Optional;
import java.util.UUID;

public interface TaskService {

    @Nullable Task save(@NotNull final Task task);

    @NotNull Optional<Task> findById(@NotNull final UUID uuid);

    @Nullable Iterable<Task> saveAll(@NotNull final Iterable<Task> tasks);

    @Nullable Iterable<Task> findAll();

    @Nullable Iterable<Task> findAllById(@NotNull final Iterable<UUID> uuids);

    boolean existsById(@NotNull final UUID uuid);

    boolean countById(@NotNull final UUID id);

    long count();

    void deleteById(@NotNull final UUID uuid);

    void delete(@NotNull final Task entity);

    void deleteAll(@NotNull final Iterable<? extends Task> entities);

    void deleteAll();
}
