package ru.parshin.tm.api.repository;

import org.springframework.data.repository.CrudRepository;
import ru.parshin.tm.domain.entity.Task;

import java.util.UUID;

public interface TaskRepository extends CrudRepository<Task, UUID> {

//    @NotNull Optional<Task> findByIdAndUser_idAndProject_id(final UUID id, final UUID userId, final UUID projectId);

//    @Nullable List<Task> findByUser_idAndProject_id(final UUID userId, final UUID projectId);

    boolean countById(UUID id);
}
