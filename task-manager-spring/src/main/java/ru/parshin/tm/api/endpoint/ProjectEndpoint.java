package ru.parshin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectEndpoint {

    @WebMethod
    Project createProject(
            @WebParam(name = "name")
            @Nullable final String projectName);

    @WebMethod
    Project readProject(
            @WebParam(name = "id")
            @Nullable final String projectId);

    @WebMethod
    List<Project> readAllProjects();

    @WebMethod
    Project updateProject(
            @WebParam(name = "project")
            @Nullable final Project project);

    @WebMethod
    void deleteProject(
            @WebParam(name = "id")
            @Nullable final String projectId);
}
