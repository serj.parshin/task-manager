package ru.parshin.tm.api.repository;

import org.springframework.data.repository.CrudRepository;
import ru.parshin.tm.domain.entity.Project;

import java.util.UUID;

public interface ProjectRepository extends CrudRepository<Project, UUID> {

//    @Nullable List<Project> findByUser_id(@NotNull final UUID userId);

//    @NotNull Optional<Project> findByIdAndUser_id(@NotNull final UUID id, @NotNull final UUID userId);

    boolean countById(UUID id);
}
