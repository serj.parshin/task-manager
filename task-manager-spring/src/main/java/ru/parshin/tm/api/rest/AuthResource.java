package ru.parshin.tm.api.rest;

import com.sun.net.httpserver.Authenticator;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/auth")
public interface AuthResource {

    @GetMapping(value = "/login", produces = "application/json")
    Authenticator.Result login(@RequestParam("username") String username, @RequestParam("password") String password);

    @GetMapping(value = "/profile", produces = "application/json")
    User profile();

    @GetMapping(value = "/logout", produces = "application/json")
    Authenticator.Result logout();
}
