package ru.parshin.tm.api.rest;

import org.springframework.web.bind.annotation.*;
import ru.parshin.tm.domain.dto.ProjectDTO;

public interface ProjectController {

    @GetMapping(value = "/{id}", produces = "application/json")
    ProjectDTO get(@PathVariable("id") String id);

    @GetMapping(value = "/all", produces = "application/json")
    Iterable<ProjectDTO> getAll();

    @PostMapping(produces = "application/json", consumes = "application/json")
    ProjectDTO post(@RequestBody ProjectDTO projectDTO);

    @PutMapping(produces = "application/json", consumes = "application/json")
    void put(@RequestBody ProjectDTO projectDTO);

    @DeleteMapping(value = "/{id}")
    void delete(@PathVariable("id") String id);
}
