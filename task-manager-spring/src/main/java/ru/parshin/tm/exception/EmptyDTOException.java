package ru.parshin.tm.exception;

public class EmptyDTOException extends RuntimeException {
    public EmptyDTOException() {
        super("Получн пустой DTO объект.");
    }
}
