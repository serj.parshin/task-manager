package ru.parshin.tm.exception;

public class ProjectNotFoundException extends RuntimeException {
    public ProjectNotFoundException() {
        super("Проект не найден.");
    }
}
