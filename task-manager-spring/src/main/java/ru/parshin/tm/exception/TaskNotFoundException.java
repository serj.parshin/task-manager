package ru.parshin.tm.exception;

public class TaskNotFoundException extends RuntimeException {
    public TaskNotFoundException() {
        super("Задача не найдена.");
    }
}
