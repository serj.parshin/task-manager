package ru.parshin.tm.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.domain.embedded.Stage;
import ru.parshin.tm.domain.entity.Task;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@Getter
@Setter
@ManagedBean(name = "taskBean")
@SessionScoped
public class TaskManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private TaskService taskService;

    private Task task;

    public Iterable<Task> list() {
        return taskService.findAll();
    }

    public String editTask(@Nullable final Task task) {
        this.task = task;
        return "pretty:editTask";
    }

    public String newTask() {
        this.task = new Task();
        return "pretty:createTask";
    }

    public Stage[] getStages() {
        return Stage.values();
    }

    public String saveTask() {
        if (this.task != null) taskService.save(task);
        task = null;
        return "pretty:taskList";
    }

    public void taskDelete(@NotNull final Task task) {
        taskService.delete(task);
    }
}
