package ru.parshin.tm.faces;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.domain.embedded.Stage;
import ru.parshin.tm.domain.entity.Project;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@Getter
@Setter
@ManagedBean(name = "projectBean")
@SessionScoped
public class ProjectManagedBean extends SpringBeanAutowiringSupport {

    @Autowired
    private ProjectService projectService;

    private Project project;

    public Iterable<Project> list() {
        return projectService.findAll();
    }

    public String editProject(@Nullable final Project project) {
        this.project = project;
        return "pretty:editProject";
    }

    public String newProject() {
        this.project = new Project();
        return "pretty:createProject";
    }

    public Stage[] getStages() {
        return Stage.values();
    }

    public String saveProject() {
        if (this.project != null) projectService.save(project);
        project = null;
        return "pretty:projectList";
    }

    public void projectDelete(@NotNull final Project project) {
        projectService.delete(project);
    }
}
