package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.parshin.tm.api.repository.ProjectRepository;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.domain.entity.Project;

import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    public Project save(@NotNull Project entity) {
        return projectRepository.save(entity);
    }

    @NotNull
    public Optional<Project> findById(@NotNull UUID uuid) {
        return projectRepository.findById(uuid);
    }

    public Iterable<Project> saveAll(@NotNull Iterable<Project> entities) {
        return projectRepository.saveAll(entities);
    }

    public Iterable<Project> findAllById(@NotNull Iterable<UUID> uuids) {
        return projectRepository.findAllById(uuids);
    }

    public Iterable<Project> findAll() {
        return projectRepository.findAll();
    }

    public boolean existsById(@NotNull UUID uuid) {
        return projectRepository.existsById(uuid);
    }

    public boolean countById(@NotNull UUID uuid) {
        return projectRepository.countById(uuid);
    }

    public long count() {
        return projectRepository.count();
    }

    public void deleteById(@NotNull UUID uuid) {
        projectRepository.deleteById(uuid);
    }

    public void delete(@NotNull Project entity) {
        projectRepository.delete(entity);
    }

    public void deleteAll(@NotNull Iterable<? extends Project> entities) {
        projectRepository.deleteAll(entities);
    }

    public void deleteAll() {
        projectRepository.deleteAll();
    }
}
