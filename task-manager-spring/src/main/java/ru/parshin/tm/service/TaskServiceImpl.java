package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.parshin.tm.api.repository.TaskRepository;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.domain.entity.Task;

import java.util.Optional;
import java.util.UUID;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public Task save(@NotNull Task task) {
        return taskRepository.save(task);
    }

    @NotNull
    public Optional<Task> findById(@NotNull UUID uuid) {
        return taskRepository.findById(uuid);
    }

    public Iterable<Task> saveAll(@NotNull Iterable<Task> tasks) {
        return taskRepository.saveAll(tasks);
    }

    public Iterable<Task> findAll() {
        return taskRepository.findAll();
    }

    public Iterable<Task> findAllById(@NotNull Iterable<UUID> uuids) {
        return taskRepository.findAllById(uuids);
    }

    public boolean existsById(@NotNull UUID uuid) {
        return taskRepository.existsById(uuid);
    }

    public boolean countById(@NotNull UUID id) {
        return taskRepository.countById(id);
    }

    public long count() {
        return taskRepository.count();
    }

    public void deleteById(@NotNull UUID uuid) {
        taskRepository.deleteById(uuid);
    }

    public void delete(@NotNull Task entity) {
        taskRepository.delete(entity);
    }

    public void deleteAll(@NotNull Iterable<? extends Task> entities) {
        taskRepository.deleteAll(entities);
    }

    public void deleteAll() {
        taskRepository.deleteAll();
    }
}
