package ru.parshin.tm.configuration;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.parshin.tm.api.client.ProjectClient;
import ru.parshin.tm.api.client.TaskClient;

@Configuration
@EnableWebMvc
@EnableFeignClients
@EnableTransactionManagement
@ComponentScan(basePackages = "ru.parshin.tm")
@EnableJpaRepositories("ru.parshin.tm.api.repository")
@PropertySource(value = {"classpath:application.properties"})
public class WebAppConfig implements WebMvcConfigurer {


    @Bean
    public TaskClient getTaskClient() {
        return TaskClient.client("http://localhost:8080/api");
    }

    @Bean
    public ProjectClient getProjectClient() {
        return ProjectClient.client("http://localhost:8080/api");
    }

    @Bean
    public InternalResourceViewResolver resolver() {

        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();

        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");

        return resolver;
    }
}
