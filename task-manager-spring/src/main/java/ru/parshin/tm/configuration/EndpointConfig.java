package ru.parshin.tm.configuration;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.endpoint.TaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
public class EndpointConfig {

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint projectEndpoint(ProjectEndpoint projectEndpoint) {
        EndpointImpl endpoint = new EndpointImpl(springBus(), projectEndpoint);
        endpoint.publish("/project");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpoint(TaskEndpoint taskEndpoint) {
        EndpointImpl endpoint = new EndpointImpl(springBus(), taskEndpoint);
        endpoint.publish("/task");
        return endpoint;
    }
}
