package ru.parshin.tm.configuration;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class DataSourceConfig {

    @Bean
    HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance();
    }

    @Bean
    public DataSource dataSource(
            @Value("${datasource.driver}") final String dataSourceDriver,
            @Value("${datasource.url}") final String dataSourceUrl,
            @Value("${datasource.user}") final String dataSourceUser,
            @Value("${datasource.password}") final String dataSourcePassword
    ) {

        final DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);

        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource,
            @Value("${hibernate.show_sql}") final boolean showSql,
            @Value("${hibernate.hbm2ddl.auto}") final String tableStrategy,
            @Value("${hibernate.dialect}") final String dialect,
            @Value("${hibernate.cache.use_second_level_cache}") final String useSecondLevelCache,
            @Value("${hibernate.cache.use_query_cache}") final String useQueryCache,
            @Value("${hibernate.cache.use_minimal_puts}") final String useMinimalPuts,
            @Value("${hibernate.cache.hazelcast.use_lite_member}") final String useLiteMember,
            @Value("${hibernate.cache.hazelcast.config}") final String hazelcastConfig,
            @Value("${hibernate.cache.region.factory_class}") final String regionFactory,
            @Value("${hibernate.cache.region_prefix}") final String regionPrefix
    ) {

        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();

        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.parshin.tm.domain");

        final Properties properties = new Properties();

        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);
        properties.put("hibernate.cache.use_second_level_cache", useSecondLevelCache);
        properties.put("hibernate.cache.use_query_cache", useQueryCache);
        properties.put("hibernate.cache.use_minimal_puts", useMinimalPuts);
        properties.put("hibernate.cache.hazelcast.use_lite_member", useLiteMember);
        properties.put("hibernate.cache.hazelcast.config", hazelcastConfig);
        properties.put("hibernate.cache.region.factory_class", regionFactory);
        properties.put("hibernate.cache.region_prefix", regionPrefix);

        factoryBean.setJpaProperties(properties);

        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBean.getObject());
        return transactionManager;
    }
}
