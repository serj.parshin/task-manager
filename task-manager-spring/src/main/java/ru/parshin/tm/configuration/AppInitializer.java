package ru.parshin.tm.configuration;

import com.sun.faces.config.FacesInitializer;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class AppInitializer extends FacesInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        final AnnotationConfigWebApplicationContext root;

        root = new AnnotationConfigWebApplicationContext();
        root.register(WebAppConfig.class);
        root.setServletContext(servletContext);

        servletContext.addListener(new ContextLoaderListener(root));
        servletContext.addListener(new RequestContextListener());

        ServletRegistration.Dynamic dispatcher
                = servletContext.addServlet("dispatcher", new CXFServlet());
        dispatcher.addMapping("/ws/*");

        ServletRegistration.Dynamic rest
                = servletContext.addServlet("rest", new DispatcherServlet());
        rest.addMapping("/api/*");
    }
}
