package ru.parshin.tm.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.embedded.Stage;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_project", schema = "task_manager")
@EqualsAndHashCode(of = "id", callSuper = true)
@NoArgsConstructor
@NamedEntityGraph(name = "project-graph", attributeNodes = {@NamedAttributeNode("tasks")})
public class Project extends AbstractEntity {

    public Project(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Stage stage;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateBegin;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateEnd;

    @Nullable
    @OneToMany(mappedBy = "project", cascade = {CascadeType.REMOVE})
    private List<Task> tasks;

    public void setTasks(Iterable<Task> tasks) {
        this.tasks = new ArrayList<>();
        tasks.forEach(this.tasks::add);
    }

    @Nullable
    public ProjectDTO toDTO() {
        try {
            final ProjectDTO dto = new ProjectDTO();
            dto.setId(getId());
            dto.setDateCreate(getDateCreate());
            dto.setName(getName());
            dto.setDescription(getDescription());
            dto.setDateBegin(getDateBegin());
            dto.setDateEnd(getDateEnd());
            dto.setStage(getStage());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
