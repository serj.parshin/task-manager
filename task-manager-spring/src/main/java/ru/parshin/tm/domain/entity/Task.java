package ru.parshin.tm.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.parshin.tm.domain.dto.TaskDTO;
import ru.parshin.tm.domain.embedded.Stage;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "app_task", schema = "task_manager")
@NoArgsConstructor
@EqualsAndHashCode(of = "id", callSuper = true)
@NamedEntityGraph(
        name = "task-graph",
        attributeNodes = {@NamedAttributeNode(value = "project", subgraph = "project")},
        subgraphs = @NamedSubgraph(name = "project", attributeNodes = @NamedAttributeNode("name"))
)
public class Task extends AbstractEntity {

    public Task(@Nullable String name) {
        this.name = name;
    }

    public Task(@Nullable Project project, @Nullable String name) {
        this.project = project;
        this.name = name;
    }

    @Nullable
    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    @ManyToOne(cascade = {CascadeType.DETACH})
    @JoinColumn(name = "project_id")
    private Project project;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Stage stage;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateBegin;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateEnd;

    @Nullable
    public TaskDTO toDTO() {
        try {
            final TaskDTO dto = new TaskDTO();
            dto.setId(getId());
            dto.setDateCreate(getDateCreate());
            if (getProject() != null) dto.setProjectId(getProject().getId());
            dto.setName(getName());
            dto.setDescription(getDescription());
            dto.setDateBegin(getDateBegin());
            dto.setDateEnd(getDateEnd());
            dto.setStage(getStage());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
