package ru.parshin.tm.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.parshin.tm.domain.embedded.Stage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(of = "id", callSuper = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskDTO extends BasedDTO implements Serializable {

    private UUID userId;

    private UUID projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Stage stage = Stage.SCHEDULED;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateBegin;

    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateEnd;
}
