package ru.parshin.tm.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.embedded.Name;
import ru.parshin.tm.domain.embedded.Role;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Getter
@Setter
@EqualsAndHashCode(of = "id", callSuper = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDTO extends BasedDTO {

    @Nullable
    private String login;

    @Nullable
    private String email;

    @Nullable
    private String passwordHash;

    @Nullable
    private Name name;

    @Nullable
    private String phone;

    @Nullable
    private Role role;

    private boolean locked;
}
