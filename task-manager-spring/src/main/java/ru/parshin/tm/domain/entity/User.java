package ru.parshin.tm.domain.entity;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.parshin.tm.domain.dto.UserDTO;
import ru.parshin.tm.domain.embedded.Name;
import ru.parshin.tm.domain.embedded.Role;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@Cacheable
@Entity
@Table(name = "app_user", schema = "task_manager")
@EqualsAndHashCode(of = "id", callSuper = true)
public class User extends AbstractEntity implements UserDetails {

    @Nullable
    @Column(unique = true)
    private String login;

    @Nullable
    @Column(unique = true)
    private String email;

    @Nullable
    private String passwordHash;

    @Nullable
    private Name name;

    @Nullable
    @Column(unique = true)
    private String phone;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.GUEST;

    @Nullable
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true, cascade = CascadeType.REMOVE)
    transient private List<Project> projects;

    private boolean locked;

    @Nullable
    public UserDTO toDTO() {
        try {
            final UserDTO dto = new UserDTO();
            dto.setId(getId());
            dto.setDateCreate(getDateCreate());
            dto.setLogin(getLogin());
            dto.setPasswordHash(getPasswordHash());
            dto.setName(getName());
            dto.setEmail(getEmail());
            dto.setPhone(getPhone());
            dto.setRole(getRole());
            dto.setLocked(isLocked());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> auths = new java.util.ArrayList<SimpleGrantedAuthority>();
        auths.add(new SimpleGrantedAuthority(role.name()));
        return auths;
    }

    @Override
    public String getPassword() {
        return passwordHash;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
