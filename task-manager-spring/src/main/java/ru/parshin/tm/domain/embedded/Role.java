package ru.parshin.tm.domain.embedded;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum Role implements Serializable {

    GUEST("Гость"),
    USER("Пользователь"),
    ADMIN("Администратор");

    @Nullable
    private String displayName;
}
