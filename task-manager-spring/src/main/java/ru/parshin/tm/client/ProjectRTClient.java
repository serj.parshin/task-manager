package ru.parshin.tm.client;

import net.bytebuddy.utility.RandomString;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.parshin.tm.domain.dto.ProjectDTO;

import java.util.List;

public class ProjectRTClient {

    private static final RestTemplate restTemplate = new RestTemplate();

    public static ProjectDTO get(String id) {
        final String url = "http://localhost:8080/api/project/" + id;
        return restTemplate.getForObject(url, ProjectDTO.class, id);
    }

    public static Iterable<ProjectDTO> getAll() {
        final ResponseEntity<List<ProjectDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/project/all", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<ProjectDTO>>() {
                }
        );
        return response.getBody();
    }

    public static ProjectDTO save(ProjectDTO projectDTO) {
        final String url = "http://localhost:8080/api/project/";
        final HttpEntity<ProjectDTO> request = new HttpEntity<>(projectDTO);
        return restTemplate.postForEntity(url, request, ProjectDTO.class).getBody();
    }

    public static void update(ProjectDTO projectDTO) {
        final String url = "http://localhost:8080/api/project/";
        final HttpEntity<ProjectDTO> request = new HttpEntity<>(projectDTO);
        restTemplate.put(url, request, ProjectDTO.class);
    }

    public static void delete(String id) {
        final String url = "http://localhost:8080/api/project/" + id;
        restTemplate.delete(url);
    }

    public static void main(String[] args) {

        final String PROJECT_ID = "e25af1cc-621c-4ce9-a2e2-463c4aeb8d2b";

        System.out.println("-------------------------get one ProjectDTO:");
        System.out.println(get(PROJECT_ID));

        System.out.println("-------------------------get all ProjectDTO:");
        System.out.println(getAll());

        System.out.println("-------------------------save one ProjectDTO:");
        ProjectDTO dto = new ProjectDTO();
        dto.setName("project from rest client");
        dto = save(dto);
        System.out.println(dto);

        System.out.println("-------------------------update one ProjectDTO:");
        ProjectDTO toUpdate = get(PROJECT_ID);
        System.out.println(toUpdate);
        toUpdate.setName("updated" + RandomString.make(3));
        update(toUpdate);
        ProjectDTO updated = get(PROJECT_ID);
        System.out.println(updated);

        System.out.println("-------------------------delete one ProjectDTO:");
        delete(dto.getId().toString());
        ProjectDTO deleted = get(dto.getId().toString());
        System.out.println(deleted);
    }
}
