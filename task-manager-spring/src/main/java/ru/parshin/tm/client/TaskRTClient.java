package ru.parshin.tm.client;

import net.bytebuddy.utility.RandomString;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.parshin.tm.domain.dto.TaskDTO;

import java.util.List;

public class TaskRTClient {

    private static final RestTemplate restTemplate = new RestTemplate();

    public static TaskDTO get(String id) {
        final String url = "http://localhost:8080/api/task/" + id;
        return restTemplate.getForObject(url, TaskDTO.class, id);
    }

    public static Iterable<TaskDTO> getAll() {
        final ResponseEntity<List<TaskDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/task/all", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<TaskDTO>>() {
                }
        );
        return response.getBody();
    }

    public static TaskDTO save(TaskDTO taskDTO) {
        final String url = "http://localhost:8080/api/task/";
        final HttpEntity<TaskDTO> request = new HttpEntity<>(taskDTO);
        return restTemplate.postForEntity(url, request, TaskDTO.class).getBody();
    }

    public static void update(TaskDTO taskDTO) {
        final String url = "http://localhost:8080/api/task/";
        final HttpEntity<TaskDTO> request = new HttpEntity<>(taskDTO);
        restTemplate.put(url, request, TaskDTO.class);
    }

    public static void delete(String id) {
        final String url = "http://localhost:8080/api/task/" + id;
        restTemplate.delete(url);
    }

    public static void main(String[] args) {

        final String TASK_ID = "190a601f-6609-45d2-813c-35edb44488fc";

        System.out.println("-------------------------get one TaskDTO:");
        System.out.println(get(TASK_ID));

        System.out.println("-------------------------get all TaskDTO:");
        System.out.println(getAll());

        System.out.println("-------------------------save one TaskDTO:");
        TaskDTO dto = new TaskDTO();
        dto.setName("task from rest client");
        dto = save(dto);
        System.out.println(dto);

        System.out.println("-------------------------update one TaskDTO:");
        TaskDTO toUpdate = get(TASK_ID);
        System.out.println(toUpdate);
        toUpdate.setName("updated" + RandomString.make(3));
        update(toUpdate);
        TaskDTO updated = get(TASK_ID);
        System.out.println(updated);

        System.out.println("-------------------------delete one TaskDTO:");
        delete(dto.getId().toString());
        TaskDTO deleted = get(dto.getId().toString());
        System.out.println(deleted);
    }
}
