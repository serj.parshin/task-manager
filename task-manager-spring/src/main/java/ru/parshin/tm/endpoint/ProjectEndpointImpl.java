package ru.parshin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.exception.ProjectNotFoundException;

import javax.jws.WebService;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@WebService(endpointInterface = "ru.parshin.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    public Project createProject(@Nullable String projectName) {
        return projectService.save(new Project(projectName));
    }

    @Override
    public Project readProject(@Nullable String projectId) {
        return projectService.findById(UUID.fromString(projectId)).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public List<Project> readAllProjects() {
        return StreamSupport.stream(projectService.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @Override
    public Project updateProject(@Nullable Project project) {
        return projectService.save(project);
    }

    @Override
    public void deleteProject(@Nullable String projectId) {
        projectService.deleteById(UUID.fromString(projectId));
    }
}
