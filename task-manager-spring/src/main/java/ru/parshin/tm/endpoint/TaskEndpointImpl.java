package ru.parshin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.parshin.tm.api.endpoint.TaskEndpoint;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.Task;
import ru.parshin.tm.exception.ProjectNotFoundException;
import ru.parshin.tm.exception.TaskNotFoundException;

import javax.jws.WebService;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@WebService(endpointInterface = "ru.parshin.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;

    @Override
    public Task createTask(@Nullable String projectId, @Nullable String name) {
        final Project project = projectService.findById(UUID.fromString(projectId)).orElseThrow(ProjectNotFoundException::new);
        return taskService.save(new Task(project, name));
    }

    @Override
    public Task readTask(@Nullable String projectId, @Nullable String taskId) {
        return taskService.findById(UUID.fromString(taskId)).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public List<Task> readAllTasks(@Nullable String projectId, @Nullable String orderBy) {
        return StreamSupport.stream(taskService.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @Override
    public Task updateTask(@Nullable Task task) {
        return taskService.save(task);
    }

    @Override
    public void deleteTask(@Nullable Task task) {
        taskService.delete(task);
    }
}
