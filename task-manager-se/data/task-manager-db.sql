-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.23 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных task-manager
CREATE DATABASE IF NOT EXISTS `task-manager` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `task-manager`;

-- Дамп структуры для таблица task-manager.app_project
CREATE TABLE IF NOT EXISTS `app_project` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `dateBegin` datetime DEFAULT NULL,
  `dateEnd` datetime DEFAULT NULL,
  `dateCreate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__app_user_project` (`user_id`),
  CONSTRAINT `FK__app_user_project` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица проектов';

-- Дамп данных таблицы task-manager.app_project: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `app_project` DISABLE KEYS */;
INSERT INTO `app_project` (`id`, `user_id`, `name`, `description`, `stage`, `dateBegin`, `dateEnd`, `dateCreate`) VALUES
	('8a6c06ef-0150-4a81-a6c1-6c131f3984c1', '7f58bdee-3153-4f22-870c-821199deffba', 'first-test-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:28:45', '2019-07-25 10:28:46', '2019-07-25 10:28:42'),
	('9efc4517-af40-4ded-ad71-a18bc9aa2dac', 'a14d3380-30a0-469a-9b7f-55163a61ae0f', 'first-admin-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:09:06', '2019-07-25 10:09:12', '2019-07-25 10:09:16'),
	('e25af1cc-621c-4ce9-a2e2-463c4aeb8d2b', '7f58bdee-3153-4f22-870c-821199deffba', 'second-test-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:29:32', '2019-07-25 10:29:33', '2019-07-25 10:29:30'),
	('ef9dcc33-1d8b-4b52-8a1e-8d576e7dda5a', 'a14d3380-30a0-469a-9b7f-55163a61ae0f', 'second-admin-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:11:31', '2019-07-25 10:11:32', '2019-07-25 10:11:33');
/*!40000 ALTER TABLE `app_project` ENABLE KEYS */;

-- Дамп структуры для таблица task-manager.app_session
CREATE TABLE IF NOT EXISTS `app_session` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `dateCreate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__app_user_session` (`user_id`),
  CONSTRAINT `FK__app_user_session` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица сессий';

-- Дамп данных таблицы task-manager.app_session: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `app_session` DISABLE KEYS */;
INSERT INTO `app_session` (`id`, `user_id`, `signature`, `dateCreate`) VALUES
	('55b1b07a-8250-4a01-8300-3278b53dffae', NULL, '1fe368584033be7921510c160f260574', '2019-07-25 10:56:40'),
	('60da8026-6319-40ab-a21e-3154edcd7bd2', NULL, 'eba067011af4c60e082df4af74116d55', '2019-07-25 10:57:53'),
	('9a649487-8ab7-4c7e-b86a-3fc303a74c7b', 'a14d3380-30a0-469a-9b7f-55163a61ae0f', 'dbc67e00b21000bda75c25c29c176023', '2019-07-25 15:03:52'),
	('cae71ad9-ec5f-4095-a169-a1907561ebec', NULL, '2a273555781986909d15176116dc6cca', '2019-07-25 11:03:49');
/*!40000 ALTER TABLE `app_session` ENABLE KEYS */;

-- Дамп структуры для таблица task-manager.app_task
CREATE TABLE IF NOT EXISTS `app_task` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `project_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `dateBegin` datetime DEFAULT NULL,
  `dateEnd` datetime DEFAULT NULL,
  `dateCreate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__app_project_task` (`project_id`),
  KEY `FK__app_user_task` (`user_id`),
  CONSTRAINT `FK__app_project_task` FOREIGN KEY (`project_id`) REFERENCES `app_project` (`id`),
  CONSTRAINT `FK__app_user_task` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица задач.';

-- Дамп данных таблицы task-manager.app_task: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `app_task` DISABLE KEYS */;
INSERT INTO `app_task` (`id`, `user_id`, `project_id`, `name`, `description`, `stage`, `dateBegin`, `dateEnd`, `dateCreate`) VALUES
	('08e3d17a-96c6-4606-9151-e26ba0c22157', '7f58bdee-3153-4f22-870c-821199deffba', '8a6c06ef-0150-4a81-a6c1-6c131f3984c1', 'second-task-in-first-test-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:45:37', '2019-07-25 10:45:37', '2019-07-25 10:45:36'),
	('11d36a71-fa2f-42f1-bde1-a0e61ee5c4a1', 'a14d3380-30a0-469a-9b7f-55163a61ae0f', 'ef9dcc33-1d8b-4b52-8a1e-8d576e7dda5a', 'second-task-in-second-admin-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:44:15', '2019-07-25 10:44:15', '2019-07-25 10:44:14'),
	('190a601f-6609-45d2-813c-35edb44488fc', 'a14d3380-30a0-469a-9b7f-55163a61ae0f', '9efc4517-af40-4ded-ad71-a18bc9aa2dac', 'secondtask-in-first-admin-project', 'Новое описание для задачи', 'SCHEDULED', '2019-07-25 10:43:32', '2019-07-25 10:43:32', '2019-07-25 10:43:31'),
	('29f8bad3-2916-4d38-a5a4-a3dc835f11fb', '7f58bdee-3153-4f22-870c-821199deffba', 'e25af1cc-621c-4ce9-a2e2-463c4aeb8d2b', 'second-task-in-second-test-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:36:36', '2019-07-25 10:36:37', '2019-07-25 10:36:34'),
	('4150d6d5-c1b0-4404-a06c-e65f6fb58702', 'a14d3380-30a0-469a-9b7f-55163a61ae0f', '9efc4517-af40-4ded-ad71-a18bc9aa2dac', 'first-task-in-first-admin-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:42:03', '2019-07-25 10:42:04', '2019-07-25 10:42:02'),
	('7c52298a-f651-4ed6-b7da-58029b70340a', '7f58bdee-3153-4f22-870c-821199deffba', '8a6c06ef-0150-4a81-a6c1-6c131f3984c1', 'first-task-in-first-test-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:37:28', '2019-07-25 10:37:30', '2019-07-25 10:37:28'),
	('bccecc7a-d0aa-4243-bc30-a513c1d3c5e7', 'a14d3380-30a0-469a-9b7f-55163a61ae0f', 'ef9dcc33-1d8b-4b52-8a1e-8d576e7dda5a', 'first-task-in-second-admin-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:39:35', '2019-07-25 10:39:36', '2019-07-25 10:39:34'),
	('cd8cb600-6cce-4354-8a0a-01a49c278778', '7f58bdee-3153-4f22-870c-821199deffba', 'e25af1cc-621c-4ce9-a2e2-463c4aeb8d2b', 'first-task-in-second-test-project', 'Какой-то дескрипшен', 'SCHEDULED', '2019-07-25 10:35:24', '2019-07-25 10:35:25', '2019-07-25 10:35:22');
/*!40000 ALTER TABLE `app_task` ENABLE KEYS */;

-- Дамп структуры для таблица task-manager.app_user
CREATE TABLE IF NOT EXISTS `app_user` (
  `id` varchar(255) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `passwordHash` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `dateCreate` datetime DEFAULT NULL,
  `locked` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`),
  KEY `name` (`firstName`,`lastName`,`middleName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица с пользователями';

-- Дамп данных таблицы task-manager.app_user: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `app_user` DISABLE KEYS */;
INSERT INTO `app_user` (`id`, `login`, `email`, `passwordHash`, `firstName`, `lastName`, `middleName`, `phone`, `role`, `dateCreate`, `locked`) VALUES
	('7f58bdee-3153-4f22-870c-821199deffba', 'test', 'test@test.test', '098F6BCD4621D373CADE4E832627B4F6', 'Test', 'Test', 'Test', '89998888666', 'USER', '2019-07-23 18:53:47', b'0'),
	('a14d3380-30a0-469a-9b7f-55163a61ae0f', 'admin', 'admin@admin.admin', '21232F297A57A5A743894A0E4A801FC3', 'Admin', 'Admin', 'Admin', '89998888777', 'ADMIN', '2019-07-23 18:53:47', b'0');
/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
