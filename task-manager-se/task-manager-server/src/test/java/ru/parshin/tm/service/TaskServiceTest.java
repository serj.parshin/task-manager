package ru.parshin.tm.service;

import org.junit.jupiter.api.Test;
import ru.parshin.tm.domain.entity.Task;
import ru.parshin.tm.exception.TaskNotFoundException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

class TaskServiceTest extends BaseServiceTest {

    @Test
    void create() {

        assertNotNull(testTask);
        assertEquals(testUser.getId(), testTask.getUser().getId());
        assertEquals(testProject.getId(), testTask.getProject().getId());
        assertEquals(testTaskName, testTask.getName());
    }

    @Test
    void select() {

        final Task task = taskService.select(testUser.getId(), testProject.getId(), testTask.getId());

        assertNotNull(task);
        assertEquals(testTask, task);
    }

    @Test
    void selectAll() {

        final List<Task> taskList = StreamSupport.stream(taskService.selectAll().spliterator(), false).collect(Collectors.toList());

        assertNotNull(taskList);
        assertFalse(taskList.isEmpty());
    }

    @Test
    void selectAllByUserAndProject() {

        final List<Task> taskList = taskService.selectAll(testUser.getId(), testProject.getId(), null);

        assertNotNull(taskList);
        assertFalse(taskList.isEmpty());
    }

    @Test
    void update() {

        final String newName = "new name";
        testTask.setName(newName);

        testTask = taskService.updateData(testTask.toDTO());

        assertNotNull(testTask);
        assertEquals(newName, testTask.getName());
    }

    @Test
    void delete() {

        taskService.delete(adminTask.getId());

        assertThrows(TaskNotFoundException.class, () -> taskService.select(adminUser.getId(), adminProject.getId(), adminTask.getId()));
    }
}