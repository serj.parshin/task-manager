package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import ru.parshin.tm.domain.embedded.Name;
import ru.parshin.tm.domain.entity.User;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest extends BaseServiceTest {

    @Test
    void registration() {

        assertNotNull(testUser);
        assertNotNull(testUser.getId());
        assertEquals(testUserLogin, testUser.getLogin());
        assertEquals(testUserPasswordHash, testUser.getPassword().getHash());
    }

    @Test
    void login() {

        final User user = userService.login(testUserLogin, testUserPassword);

        assertNotNull(user);
        assertNotNull(user.getId());
        assertEquals(testUserLogin, user.getLogin());
        assertEquals(testUserPasswordHash, user.getPassword().getHash());
    }

    @Test
    void logout() {

        final @NotNull User logoutUser = userService.logout(adminUser.getId());

        assertNotNull(logoutUser);
        assertNotNull(logoutUser.getId());
        assertNotEquals(adminUserLogin, logoutUser.getLogin());
        assertNotEquals(adminUser.getPassword(), logoutUser.getPassword());
    }

    @Test
    void selectAll() {

        final List<User> userList = StreamSupport.stream(userService.selectAll().spliterator(), false).collect(Collectors.toList());

        assertNotNull(userList);
        assertFalse(userList.isEmpty());
    }

    @Test
    void update() {

        User selectedUser = userService.select(testUser.getId());
        final Name newName = new Name("new name", "new name", "new name");
        selectedUser.setName(newName);

        selectedUser = userService.updateData(selectedUser.toDTO());
        assertEquals(newName, selectedUser.getName());
    }
}