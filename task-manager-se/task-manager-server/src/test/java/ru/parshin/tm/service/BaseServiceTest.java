package ru.parshin.tm.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import ru.parshin.tm.api.repository.ProjectRepository;
import ru.parshin.tm.api.repository.SessionRepository;
import ru.parshin.tm.api.repository.TaskRepository;
import ru.parshin.tm.api.repository.UserRepository;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.api.service.SessionService;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.api.service.UserService;
import ru.parshin.tm.configuration.ApplicationConfiguration;
import ru.parshin.tm.domain.embedded.Name;
import ru.parshin.tm.domain.embedded.Password;
import ru.parshin.tm.domain.embedded.Role;
import ru.parshin.tm.domain.embedded.Stage;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.Session;
import ru.parshin.tm.domain.entity.Task;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.util.encoder.PasswordHash;

import java.time.ZonedDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ApplicationConfiguration.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("dev")
abstract class BaseServiceTest {

    @Autowired
    protected UserService userService;
    @Autowired
    protected SessionService sessionService;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected TaskService taskService;

    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected SessionRepository sessionRepository;
    @Autowired
    protected ProjectRepository projectRepository;
    @Autowired
    protected TaskRepository taskRepository;

    protected Session adminSesson;

    protected Session testUserSession;
    protected User testUser;
    protected Project testProject;
    protected Task testTask;

    protected Session adminUserSession;
    protected User adminUser;
    protected Project adminProject;
    protected Task adminTask;

    protected final String testUserLogin = "test";
    protected final String testUserPassword = testUserLogin;
    protected final String testUserPasswordHash = PasswordHash.md5(testUserPassword);
    protected final String testProjectName = "test project";
    protected final String testProjectDescription = "test project description";

    protected final String adminUserLogin = "admin";
    protected final String adminUserPassword = adminUserLogin;
    protected final String adminUserPasswordHash = PasswordHash.md5(adminUserPassword);
    protected final String adminProjectName = "admin project";
    protected final String adminProjectDescription = "admin project description";

    final String testTaskName = "test task";


    @Test
    @DisplayName("Инициализация бинов сервисов")
    void initRepos() {
        assertNotNull(userService);
        assertNotNull(sessionService);
        assertNotNull(projectService);
        assertNotNull(taskService);
    }

    @BeforeAll
    @Transactional
    void initTestData() {

        testUser = new User();
        testProject = new Project();
        testTask = new Task();

        adminSesson = new Session();
        adminUser = new User();
        adminProject = new Project();
        adminTask = new Task();

        testUser.setLogin(testUserLogin);
        testUser.setPassword(new Password(testUserPassword));
        testUser.setName(new Name("test", "test", "test"));
        testUser.setEmail("test@test.test");
        testUser.setPhone("123123123");
        testUser.setRole(Role.USER);
        testUser.setLocked(false);
        testUser.setProjects(new ArrayList<>());

        testProject.setUser(testUser);
        testProject.setName(testProjectName);
        testProject.setDescription(testProjectDescription);
        testProject.setStage(Stage.SCHEDULED);
        testProject.setDateBegin(ZonedDateTime.now());
        testProject.setDateEnd(ZonedDateTime.now());

        testTask.setUser(testUser);
        testTask.setProject(testProject);
        testTask.setName(testTaskName);
        testTask.setDescription("test task description");
        testTask.setDateBegin(ZonedDateTime.now());
        testTask.setDateEnd(ZonedDateTime.now());
        testTask.setStage(Stage.SCHEDULED);

        adminUser.setLogin(adminUserLogin);
        adminUser.setPassword(new Password(adminUserPassword));
        adminUser.setName(new Name("admin", "admin", "admin"));
        adminUser.setEmail("admin@admin.admin");
        adminUser.setPhone("9219375562378");
        adminUser.setRole(Role.ADMIN);
        adminUser.setLocked(false);

        adminProject.setUser(adminUser);
        adminProject.setName(adminProjectName);
        adminProject.setDescription(adminProjectDescription);
        adminProject.setStage(Stage.SCHEDULED);
        adminProject.setDateBegin(ZonedDateTime.now());
        adminProject.setDateEnd(ZonedDateTime.now());

        adminTask.setUser(adminUser);
        adminTask.setProject(adminProject);
        adminTask.setName("admin task");
        adminTask.setDescription("admin task description");
        adminTask.setDateBegin(ZonedDateTime.now());
        adminTask.setDateEnd(ZonedDateTime.now());
        adminTask.setStage(Stage.SCHEDULED);

        adminSesson.setUser(adminUser);
        adminSesson.sign();

        testUser = userRepository.save(testUser);
        testProject = projectRepository.save(testProject);
        testTask = taskRepository.save(testTask);

        adminUser = userRepository.save(adminUser);
        adminProject = projectRepository.save(adminProject);
        adminTask = taskRepository.save(adminTask);

        sessionRepository.save(adminSesson);
    }
}
