package ru.parshin.tm.service;

import org.junit.jupiter.api.Test;
import ru.parshin.tm.exception.SessionNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class SessionServiceTest extends BaseServiceTest {

    @Test
    void begin() {

        testUserSession = sessionService.begin(testUser);

        assertNotNull(testUserSession);
        assertEquals(testUser.getId(), testUserSession.getUser().getId());
    }

    @Test
    void terminate() {

        sessionService.terminate(adminSesson.getId());
        assertThrows(SessionNotFoundException.class, () -> sessionService.select(adminUser.getId()));
    }
}