package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.exception.ProjectNotFoundException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

class ProjectServiceTest extends BaseServiceTest {

    @Test
    void select() {

        final ProjectDTO projectDTO = projectService.select(testUser.getId(), testProject.getId()).toDTO();

        assertNotNull(projectDTO);
        assertEquals(testProject.getId(), projectDTO.getId());
        assertEquals(testProject.getUser().getId(), projectDTO.getUserId());
        assertEquals(testProject.getName(), projectDTO.getName());
        assertEquals(testProject.getDescription(), projectDTO.getDescription());
        assertEquals(testProject.getStage(), projectDTO.getStage());
    }

    @Test
    void create() {

        final String newProjectName = "new project of test user";
        final ProjectDTO newProject = projectService.create(testUser, newProjectName).toDTO();

        assertNotNull(newProject);
        assertNotNull(newProject.getUserId());
        assertEquals(testUser.getId(), newProject.getUserId());
        assertEquals(newProjectName, newProject.getName());
    }

    @Test
    void selectAll() {

        final List<ProjectDTO> projectDTOList;
        projectDTOList = StreamSupport.stream(projectService.selectAll().spliterator(), false)
                .map(Project::toDTO)
                .collect(Collectors.toList());

        assertNotNull(projectDTOList);
        assertFalse(projectDTOList.isEmpty());
    }

    @Test
    void selectAllByUserId() {

        final List<Project> projectDTOList = projectService.selectAll(testUser.getId(), null);

        assertNotNull(projectDTOList);
        assertFalse(projectDTOList.isEmpty());
    }

    @Test
    void update() {

        @Nullable final ProjectDTO projectDTO = projectService.select(testUser.getId(), testProject.getId()).toDTO();
        assertNotNull(projectDTO);

        @NotNull final String newProjectName = "new project of test user";
        projectDTO.setName(newProjectName);

        final Project updateData = projectService.updateData(projectDTO);

        assertNotNull(projectDTO);
        assertEquals(projectDTO.getId(), updateData.getId());
        assertEquals(projectDTO.getUserId(), updateData.getUser().getId());
        assertEquals(projectDTO.getName(), updateData.getName());
        assertEquals(projectDTO.getDescription(), updateData.getDescription());
        assertEquals(projectDTO.getStage(), updateData.getStage());
        assertEquals(projectDTO.getDateCreate(), updateData.getDateCreate());
        assertEquals(projectDTO.getDateBegin(), updateData.getDateBegin());
        assertEquals(projectDTO.getDateEnd(), updateData.getDateEnd());

        final Project readedProject = projectRepository.findById(testProject.getId()).orElseThrow(ProjectNotFoundException::new);
        assertNotNull(readedProject);
        assertEquals(readedProject.getId(), updateData.getId());
        assertEquals(readedProject.getName(), updateData.getName());
        assertEquals(readedProject.getDescription(), updateData.getDescription());
        assertEquals(readedProject.getStage(), updateData.getStage());
        assertEquals(readedProject.getDateCreate(), updateData.getDateCreate());
        assertEquals(readedProject.getDateBegin(), updateData.getDateBegin());
        assertEquals(readedProject.getDateEnd(), updateData.getDateEnd());
    }

    @Test
    void isExists() {

        assertTrue(projectService.isExists(testProject.getId()));
        assertFalse(projectService.isExists("invalid Id"));
    }
}