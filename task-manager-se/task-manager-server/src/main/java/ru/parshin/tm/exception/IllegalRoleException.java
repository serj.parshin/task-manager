package ru.parshin.tm.exception;

public class IllegalRoleException extends RuntimeException {
    public IllegalRoleException() {
        super("Поле \'Роль\' не может быть пустым.");
    }

    public IllegalRoleException(String role) {
        super("Роль с именем: " + role + " не существует.");
    }
}
