package ru.parshin.tm.exception;

public class IllegalLoginException extends RuntimeException {
    public IllegalLoginException() {
        super("Логин не может быть пустым!");
    }

    public IllegalLoginException(String login) {
        super("Логин: " + login + " не найден.");
    }
}
