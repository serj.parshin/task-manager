package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.parshin.tm.api.repository.UserRepository;
import ru.parshin.tm.api.service.UserService;
import ru.parshin.tm.domain.dto.UserDTO;
import ru.parshin.tm.domain.embedded.Password;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.IllegalLoginException;
import ru.parshin.tm.exception.IllegalPasswordException;
import ru.parshin.tm.exception.UserNotFoundException;

import java.util.Optional;

import static ru.parshin.tm.domain.embedded.Role.ADMIN;
import static ru.parshin.tm.domain.embedded.Role.USER;

@Transactional
@Component
public class UserServiceImpl extends AbstractService implements UserService {

    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    public User registration(@Nullable final String login, @Nullable final String password) {

        if (login == null || login.isEmpty()) throw new IllegalLoginException();
        if (password == null || password.isEmpty()) throw new IllegalPasswordException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(new Password(password));
        user.setRole(USER);
        user.setLocked(false);

        return userRepository.save(user);
    }

    @NotNull
    @Override
    public User login(@Nullable final String login, @Nullable final String password) {

        if (login == null || login.isEmpty()) throw new IllegalLoginException();
        if (password == null || password.isEmpty()) throw new IllegalPasswordException();

        @NotNull final Optional<User> optionalUser = userRepository.findByLogin(login);
        @NotNull final User user = optionalUser.orElseThrow(UserNotFoundException::new);

        @Nullable Password userPassword = user.getPassword();
        if (userPassword != null && userPassword.isEqualsTo(password)) return user;
        throw new IllegalPasswordException();
    }

    @NotNull
    @Override
    public User logout(@Nullable final String userId) {
        return new User();
    }

    @Override
    @NotNull
    public User select(@Nullable final String id) {

        if (id == null || id.isEmpty()) throw new UserNotFoundException();

        @NotNull final Optional<User> optionalUser = userRepository.findById(id);
        @NotNull final User user = optionalUser.orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Override
    public Iterable<User> selectAll() {

        return userRepository.findAll();
    }

    @Override
    public User updateData(@Nullable final UserDTO userDTO) {

        if (userDTO == null || !isExists(userDTO.getId())) throw new UserNotFoundException();

        Optional<User> toUpdate = userRepository.findById(userDTO.getId());
        User userToUpdate = toUpdate.orElseThrow(UserNotFoundException::new);

        userToUpdate.setLogin(userDTO.getLogin());
        userToUpdate.setEmail(userDTO.getEmail());
        userToUpdate.setName(userDTO.getName());
        userToUpdate.setPhone(userDTO.getPhone());

        return userRepository.save(userToUpdate);
    }

    @Override
    @NotNull
    public User updatePassword(@Nullable final String userId, @Nullable final String password) {

        if (!isExists(userId)) throw new UserNotFoundException();
        if (password == null || password.isEmpty()) throw new IllegalPasswordException();

        @NotNull final Optional<User> optionalUser = userRepository.findById(userId);
        @NotNull final User user = optionalUser.orElseThrow(UserNotFoundException::new);

        user.setPassword(new Password(password));
        userRepository.save(user);

        return user;
    }

    @Override
    public void delete(@Nullable final User user) {


        if (user == null || !isExists(user.getId())) return;
        userRepository.delete(user);
    }

    @Override
    public void delete(@Nullable final String id) {

        if (!isExists(id)) return;
        userRepository.deleteById(id);
    }

    @Override
    public boolean isAdmin(@Nullable final String id) {

        if (id == null || id.isEmpty() || !isExists(id)) return false;

        @NotNull final Optional<User> optionalUser = userRepository.findById(id);
        @NotNull final User user = optionalUser.orElseThrow(UserNotFoundException::new);

        return user.getRole().equals(ADMIN);
    }

    private boolean isExists(@Nullable final String id) {

        if (id == null || id.isEmpty()) return false;

        return userRepository.countById(id) > 0;
    }
}
