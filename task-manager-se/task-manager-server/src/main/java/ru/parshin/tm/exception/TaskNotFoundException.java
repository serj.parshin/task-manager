package ru.parshin.tm.exception;

public final class TaskNotFoundException extends RuntimeException {
    public TaskNotFoundException() {
        super("Задача не найдена.");
    }
}
