package ru.parshin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface UserEndpoint {

    @Nullable
    @WebMethod
    UserDTO registration(
            @WebParam(name = "login")
            @Nullable final String login,
            @WebParam(name = "password")
            @Nullable final String password);

    @Nullable
    @WebMethod
    UserDTO readUser(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "userId")
            @Nullable final String userId);

    @Nullable
    @WebMethod
    List<UserDTO> readAllUsers(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO);

    @Nullable
    @WebMethod
    UserDTO updateUser(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "userDTO")
            @Nullable final UserDTO userDTO);

    @WebMethod
    UserDTO updatePassword(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "password")
            @Nullable final String password);

    @WebMethod
    void deleteUser(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "userDTO")
            @Nullable final UserDTO userDTO);
}
