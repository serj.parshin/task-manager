package ru.parshin.tm.exception;

public class UserExistsException extends RuntimeException {
    public UserExistsException() {
        super("Пользователь уже существует.");
    }
}
