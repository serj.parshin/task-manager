package ru.parshin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import ru.parshin.tm.domain.entity.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends CrudRepository<Task, String> {

    @NotNull Optional<Task> findByIdAndUser_idAndProject_id(@NotNull final String id, @NotNull final String userId, @NotNull final String projectId);

    @Nullable List<Task> findByUser_idAndProject_id(@NotNull final String userId, @NotNull final String projectId);

    @NotNull Long countById(@NotNull final String id);
}
