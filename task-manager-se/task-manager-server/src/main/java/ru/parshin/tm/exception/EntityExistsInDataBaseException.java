package ru.parshin.tm.exception;

public class EntityExistsInDataBaseException extends RuntimeException{
    public EntityExistsInDataBaseException(String id) {
        super("Сущность с id: " + id + " уже существует.");
    }
}
