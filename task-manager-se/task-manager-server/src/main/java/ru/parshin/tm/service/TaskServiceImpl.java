package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.parshin.tm.api.repository.TaskRepository;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.domain.dto.TaskDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.Task;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.IllegalNameException;
import ru.parshin.tm.exception.ProjectNotFoundException;
import ru.parshin.tm.exception.TaskNotFoundException;
import ru.parshin.tm.exception.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@Transactional
@Component
public class TaskServiceImpl extends AbstractService implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Override
    public Task create(@NotNull final User user, @NotNull final Project project, @Nullable final String name) {

        if (name == null || name.isEmpty()) throw new IllegalNameException();

        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setProject(project);
        task.setName(name);

        return taskRepository.save(task);
    }

    @NotNull
    @Override
    public Task select(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {

        if (taskId == null || taskId.isEmpty() || !isExists(taskId)) throw new TaskNotFoundException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();

        @NotNull final Optional<Task> optionalTask = taskRepository.findByIdAndUser_idAndProject_id(taskId, userId, projectId);
        @NotNull final Task task = optionalTask.orElseThrow(TaskNotFoundException::new);
        return task;
    }

    @Override
    public Iterable<Task> selectAll() {

        return taskRepository.findAll();
    }

    @NotNull
    @Deprecated
    public List<Task> selectAll(@Nullable final String userId, @Nullable final String projectId, @Nullable final String orderBy) {

        final TaskDTO example = new TaskDTO();

        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();

        @NotNull Optional<List<Task>> optionalTaskDTOS = Optional.ofNullable(taskRepository.findByUser_idAndProject_id(userId, projectId));
        return optionalTaskDTOS.orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task updateData(@Nullable final TaskDTO taskDTO) {

        if (taskDTO == null || !isExists(taskDTO.getId())) throw new ProjectNotFoundException();

        final Optional<Task> toUpdate = taskRepository.findById(taskDTO.getId());
        final Task task = toUpdate.orElseThrow(TaskNotFoundException::new);

        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateBegin(taskDTO.getDateBegin());
        task.setDateEnd(taskDTO.getDateEnd());
        task.setStage(taskDTO.getStage());

        return taskRepository.save(task);
    }

    @Override
    public void delete(@Nullable final String id) {

        if (!isExists(id)) return;
        taskRepository.deleteById(id);
    }

    public boolean isExists(@Nullable final String id) {

        if (id == null || id.isEmpty()) return false;

        return taskRepository.countById(id) > 0;
    }
}
