package ru.parshin.tm.exception.service.session;

import ru.parshin.tm.exception.service.AbstractServiceException;

public class DeleteSessionException extends AbstractServiceException {

    public DeleteSessionException() {
        super("Ошибка при удалении сессии.", "004");
    }
}
