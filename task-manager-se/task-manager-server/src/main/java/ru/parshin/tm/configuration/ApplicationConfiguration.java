package ru.parshin.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = "ru.parshin.tm")
@EnableTransactionManagement
@EnableJpaRepositories("ru.parshin.tm.api.repository")
@Import(DataBaseConfiguration.class)
public class ApplicationConfiguration {
}
