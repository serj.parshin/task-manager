package ru.parshin.tm.exception;

public final class EmptyDataBaseException extends RuntimeException {
    public EmptyDataBaseException() {
        super("[DB Error]: База данных: пуста.");
    }
    public EmptyDataBaseException(Class c) {
        super("[DB Error]: База данных: " + c.getSimpleName() + " пуста.");
    }
}
