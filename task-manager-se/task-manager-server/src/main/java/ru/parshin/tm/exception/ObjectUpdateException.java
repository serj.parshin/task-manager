package ru.parshin.tm.exception;

public class ObjectUpdateException extends RuntimeException {
    public ObjectUpdateException() {
        super("Ошибка при обновлении объекта.");
    }
}
