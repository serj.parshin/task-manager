package ru.parshin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.User;

import java.util.List;

public interface ProjectService {

    @NotNull Project create(final User user, @Nullable final String name);

    @NotNull Project select(@Nullable final String userId, @Nullable final String projectId);

    @NotNull Project updateData(@Nullable final ProjectDTO projectDTO);

    Iterable<Project> selectAll();

    @NotNull List<Project> selectAll(@Nullable final String userId, @Nullable final String orderByColumn);

    void delete(@Nullable final Project project);

    void delete(@Nullable final String id);

    boolean isExists(@Nullable final String id);
}