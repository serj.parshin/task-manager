package ru.parshin.tm.exception;

public class SessionNotFoundException extends RuntimeException {
    public SessionNotFoundException() {
        super("Сессия не найдена.");
    }
}
