package ru.parshin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.parshin.tm.api.endpoint.SessionEndpoint;
import ru.parshin.tm.api.service.SessionService;
import ru.parshin.tm.api.service.UserService;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.IllegalSessionException;

import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.parshin.tm.api.endpoint.SessionEndpoint")
public class SessionEndpointImpl implements SessionEndpoint {

    @Autowired
    private SessionService sessionService;
    @Autowired
    private UserService userService;

    @Override
    public SessionDTO beginSession(@Nullable final SessionDTO sessionDTO, @Nullable final String login, @Nullable final String password) {

        if (sessionService.isActual(sessionDTO)) return sessionDTO;

        @NotNull final User user = userService.login(login, password);
        return sessionService.begin(user).toDTO();
    }

    @Override
    public void terminateSession(@Nullable final SessionDTO sessionDTO) {

        if (!sessionService.isActual(sessionDTO)) throw new IllegalSessionException();
        sessionService.terminate(sessionDTO.getId());
    }
}
