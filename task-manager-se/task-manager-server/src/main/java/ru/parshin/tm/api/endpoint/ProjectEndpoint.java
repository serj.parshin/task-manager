package ru.parshin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ProjectEndpoint {

    @WebMethod
    ProjectDTO createProject(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name") final String projectName);

    @WebMethod
    ProjectDTO readProject(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id")
            @Nullable final String projectId);

    @WebMethod
    @Nullable
    List<ProjectDTO> readAllProjects(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "orderBy")
            @Nullable final String orderBy);

    @WebMethod
    ProjectDTO updateProject(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectDTO")
            @Nullable final ProjectDTO projectDTO);

    @WebMethod
    void deleteProject(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectDTO")
            @Nullable final ProjectDTO projectDTO);
}
