package ru.parshin.tm.exception;

public final class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("Пользователь не найден.");
    }

    public UserNotFoundException(String login) {
        super("Пользователь с логином " + login + " не найден.");
    }
}
