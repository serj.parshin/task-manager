package ru.parshin.tm.exception;

public final class NullContextItem extends RuntimeException {
    public NullContextItem() {
        super("Невозмжно установить null объект в контекст.");
    }
}
