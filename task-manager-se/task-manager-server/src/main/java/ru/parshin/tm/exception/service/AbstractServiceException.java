package ru.parshin.tm.exception.service;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.exception.AbstractException;

public abstract class AbstractServiceException extends AbstractException {

    private static final String WHERE = "-service-";

    public AbstractServiceException(@NotNull String message, @NotNull String errorCode) {
        super(message, WHERE + errorCode);
    }
}
