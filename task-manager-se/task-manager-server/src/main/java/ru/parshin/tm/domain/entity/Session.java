package ru.parshin.tm.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.util.encoder.Signature;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_session")
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EqualsAndHashCode(of = "id", callSuper = true)
public class Session extends AbstractEntity {

    @Nullable
    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Nullable
    private String signature;

    public Session(@NotNull final User user) {
        this.user = user;
    }

    public void sign() {
        this.signature = Signature.sign(this);
    }

    @Nullable
    public SessionDTO toDTO() {
        try {
            final SessionDTO dto = new SessionDTO();
            dto.setId(getId());
            dto.setDateCreate(getDateCreate());
            dto.setUserId(getUser().getId());
            dto.setSignature(getSignature());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
