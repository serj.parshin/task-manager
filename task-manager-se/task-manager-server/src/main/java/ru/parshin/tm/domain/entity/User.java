package ru.parshin.tm.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.UserDTO;
import ru.parshin.tm.domain.embedded.Name;
import ru.parshin.tm.domain.embedded.Password;
import ru.parshin.tm.domain.embedded.Role;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Cacheable
@Entity
@Table(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EqualsAndHashCode(of = "id", callSuper = true)
public class User extends AbstractEntity {

    @Nullable
    @Column(unique = true)
    private String login;

    @Nullable
    @Column(unique = true)
    private String email;

    @Nullable
    private Password password;

    @Nullable
    private Name name;

    @Nullable
    @Column(unique = true)
    private String phone;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.GUEST;

    @Nullable
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true, cascade = CascadeType.REMOVE)
    transient private List<Project> projects;

    private boolean locked;

    @Nullable
    public UserDTO toDTO() {
        try {
            final UserDTO dto = new UserDTO();
            dto.setId(getId());
            dto.setDateCreate(getDateCreate());
            dto.setLogin(getLogin());
            dto.setPasswordHash(getPassword().getHash());
            dto.setName(getName());
            dto.setEmail(getEmail());
            dto.setPhone(getPhone());
            dto.setRole(getRole());
            dto.setLocked(isLocked());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
