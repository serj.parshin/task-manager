package ru.parshin.tm.exception;

public class UpdateUserException extends RuntimeException {
    public UpdateUserException() {
        super("Ошибка при обновлении пользователя.");
    }
}
