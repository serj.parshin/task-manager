package ru.parshin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import ru.parshin.tm.domain.entity.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {

    @NotNull Optional<User> findByLogin(@NotNull final String login);

    @NotNull Long countById(@NotNull final String id);
}
