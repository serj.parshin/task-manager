package ru.parshin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import ru.parshin.tm.domain.entity.Session;

import java.util.Optional;

public interface SessionRepository extends CrudRepository<Session, String> {

    @NotNull Optional<Session> findByUser_id(@NotNull final String userId);

    @NotNull Long countById(@NotNull final String id);
}
