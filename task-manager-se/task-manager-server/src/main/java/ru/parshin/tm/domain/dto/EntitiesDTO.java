package ru.parshin.tm.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EntitiesDTO implements Serializable {

    @NotNull
    @Builder.Default
    @XmlElement(name = "user")
    @XmlElementWrapper(nillable = true, name = "users")
    private final List<UserDTO> users = new ArrayList<>();

    @NotNull
    @Builder.Default
    @XmlElement(name = "project")
    @XmlElementWrapper(nillable = true, name = "projects")
    private final List<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    @Builder.Default
    @XmlElement(name = "task")
    @XmlElementWrapper(nillable = true, name = "tasks")
    private final List<TaskDTO> tasks = new ArrayList<>();
}
