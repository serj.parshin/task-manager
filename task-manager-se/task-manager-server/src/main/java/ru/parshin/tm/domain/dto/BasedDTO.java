package ru.parshin.tm.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.util.adapter.ZonedDateTimeXmlAdapter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(of = "id")
public abstract class BasedDTO {

    @Id
    @NotNull
    @XmlAttribute
    @Column(updatable = false, nullable = false)
    private String id = UUID.randomUUID().toString();

    @Nullable
    @XmlJavaTypeAdapter(ZonedDateTimeXmlAdapter.class)
    private ZonedDateTime dateCreate = ZonedDateTime.now();
}
