package ru.parshin.tm.exception;

public class SearchException extends RuntimeException {
    public SearchException() {
        super("Не заданы критерии поиска.");
    }
}
