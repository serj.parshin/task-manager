package ru.parshin.tm.exception.service.session;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.exception.service.AbstractServiceException;

public class AbstractSessionException extends AbstractServiceException {

    private static final String WITH = "[session]";

    public AbstractSessionException(@NotNull final String message, final int errorCode) {
        super(message, WITH + errorCode);
    }
}
