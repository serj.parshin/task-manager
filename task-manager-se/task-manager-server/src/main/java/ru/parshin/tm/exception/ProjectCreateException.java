package ru.parshin.tm.exception;

public class ProjectCreateException extends RuntimeException {
    public ProjectCreateException() {
        super("Ошибка при создании проекта.");
    }
}
