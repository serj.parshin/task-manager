package ru.parshin.tm.util.adapter;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.ZonedDateTime;

public class ZonedDateTimeXmlAdapter extends XmlAdapter<String, ZonedDateTime> {

    @Override
    @NotNull
    public ZonedDateTime unmarshal(@NotNull final String v) throws Exception {
        return ZonedDateTime.parse(v);
    }

    @Override
    @NotNull
    public String marshal(@NotNull final ZonedDateTime v) throws Exception {
        return v.toString();
    }
}
