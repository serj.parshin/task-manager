package ru.parshin.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.parshin.tm.api.endpoint.*;

import javax.xml.ws.Endpoint;

@Getter
@Setter
@NoArgsConstructor
@Component
public class ServerContext {

    @Autowired
    private SessionEndpoint sessionEndpoint;
    @Autowired
    private UserEndpoint userEndpoint;
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private TaskEndpoint taskEndpoint;
    @Autowired
    private SystemEndpoint systemEndpoint;
    /*
     * java -jar -Dserver.port=7070 task-manager-server.jar
     */
    @Autowired
    private ServerSettings settings;

    public void run() {

        Endpoint.publish(settings.getServerHost() + settings.getServerPort() + "/SessionEndpoint", sessionEndpoint);
        Endpoint.publish(settings.getServerHost() + settings.getServerPort() + "/UserEndpoint", userEndpoint);
        Endpoint.publish(settings.getServerHost() + settings.getServerPort() + "/ProjectEndpoint", projectEndpoint);
        Endpoint.publish(settings.getServerHost() + settings.getServerPort() + "/TaskEndpoint", taskEndpoint);
        Endpoint.publish(settings.getServerHost() + settings.getServerPort() + "/SystemEndpoint", systemEndpoint);
    }
}
