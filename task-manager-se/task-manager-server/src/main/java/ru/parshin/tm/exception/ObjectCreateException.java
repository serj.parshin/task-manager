package ru.parshin.tm.exception;

public class ObjectCreateException extends RuntimeException{
    public ObjectCreateException() {
        super("Ошибка при записи сущности в базу данных.");
    }
}
