package ru.parshin.tm.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@EqualsAndHashCode(of = "id")
abstract public class AbstractEntity {

    @Id
    @NotNull
    @Column(updatable = false, nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    private ZonedDateTime dateCreate = ZonedDateTime.now();
}
