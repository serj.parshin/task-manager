package ru.parshin.tm.exception;

public class UpdateSessionException extends RuntimeException {
    public UpdateSessionException() {
        super("Ошибка при обновлении сессии.");
    }
}
