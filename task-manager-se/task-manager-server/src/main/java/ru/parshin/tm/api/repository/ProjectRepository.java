package ru.parshin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import ru.parshin.tm.domain.entity.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends CrudRepository<Project, String> {

    @Nullable List<Project> findByUser_id(@NotNull final String userId);

    @NotNull Optional<Project> findByIdAndUser_id(@NotNull final String id, @NotNull final String userId);

    @NotNull Long countById(@NotNull final String id);
}
