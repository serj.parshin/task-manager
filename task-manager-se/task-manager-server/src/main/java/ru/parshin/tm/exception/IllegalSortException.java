package ru.parshin.tm.exception;

public class IllegalSortException extends RuntimeException {
    public IllegalSortException() {
        super("Не выбран метод сортировки.");
    }
}
