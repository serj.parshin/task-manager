package ru.parshin.tm.exception;

public class UpdateProjectException extends RuntimeException {
    public UpdateProjectException() {
        super("Ошибка при обновлении проекта.");
    }
}
