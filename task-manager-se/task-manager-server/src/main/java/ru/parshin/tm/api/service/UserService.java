package ru.parshin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.UserDTO;
import ru.parshin.tm.domain.entity.User;

public interface UserService {

    @NotNull User registration(@Nullable final String login, @Nullable final String password);

    @NotNull User login(@Nullable final String login, @Nullable final String password);

    @NotNull User logout(@Nullable final String userId);

    @NotNull User select(@Nullable final String id);

    User updateData(@Nullable final UserDTO userDTO);

    @NotNull User updatePassword(@Nullable String userId, @Nullable String password);

    Iterable<User> selectAll();

    void delete(@Nullable final User user);

    void delete(@Nullable final String id);

    boolean isAdmin(@Nullable final String id);
}
