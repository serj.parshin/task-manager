package ru.parshin.tm.exception;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public abstract class AbstractException extends RuntimeException {

    private final String errorCode;

    public AbstractException(@NotNull final String message, @NotNull final String errorCode) {
        super("[ ERROR ] - " + errorCode + " - " + message);
        this.errorCode = errorCode;
    }
}
