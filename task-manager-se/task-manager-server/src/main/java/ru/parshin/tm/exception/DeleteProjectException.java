package ru.parshin.tm.exception;

public class DeleteProjectException extends RuntimeException {
    public DeleteProjectException() {
        super("Ошибка при удалении проекта.");
    }
}
