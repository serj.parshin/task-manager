package ru.parshin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.entity.Session;
import ru.parshin.tm.domain.entity.User;

public interface SessionService {

    @Nullable Session begin(@Nullable final User user);

    @Nullable Session select(@Nullable final String userId);

    void terminate(@Nullable final String sessionId);

    boolean isActual(@Nullable final SessionDTO sessionDTO);
}
