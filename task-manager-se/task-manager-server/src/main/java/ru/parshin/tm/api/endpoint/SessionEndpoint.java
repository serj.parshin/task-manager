package ru.parshin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface SessionEndpoint {

    @WebMethod
    SessionDTO beginSession(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login")
            @Nullable final String login,
            @WebParam(name = "password")
            @Nullable final String password);

    @WebMethod
    void terminateSession(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO);
}
