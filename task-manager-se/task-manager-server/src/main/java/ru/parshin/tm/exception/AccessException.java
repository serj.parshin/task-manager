package ru.parshin.tm.exception;

public class AccessException extends RuntimeException {
    public AccessException() {
        super("Ошибка доступа");
    }

    public AccessException(String message) {
        super(message);
    }
}
