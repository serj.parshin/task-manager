package ru.parshin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.api.service.SessionService;
import ru.parshin.tm.api.service.UserService;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.IllegalSessionException;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@WebService(endpointInterface = "ru.parshin.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private SessionService sessionService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private UserService userService;

    @Override
    @Nullable
    public ProjectDTO createProject(@Nullable final SessionDTO sessionDTO, @Nullable final String projectName) {

        @NotNull final User user = userService.select(sessionDTO.getUserId());
        if (sessionService.isActual(sessionDTO))
            return projectService.create(user, projectName).toDTO();
        throw new IllegalSessionException();

    }

    @Override
    @Nullable
    public ProjectDTO readProject(@Nullable final SessionDTO sessionDTO, @Nullable final String projectId) {

        if (sessionService.isActual(sessionDTO))
            return projectService.select(sessionDTO.getUserId(), projectId).toDTO();
        throw new IllegalSessionException();

    }

    @Override
    @Nullable
    public List<ProjectDTO> readAllProjects(@Nullable final SessionDTO sessionDTO, @Nullable final String orderBy) {

        if (sessionService.isActual(sessionDTO))
            return projectService
                    .selectAll(sessionDTO.getUserId(), orderBy)
                    .stream().map(Project::toDTO)
                    .collect(Collectors.toList());
        throw new IllegalSessionException();

    }

    @Override
    @Nullable
    public ProjectDTO updateProject(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) {

        if (sessionService.isActual(sessionDTO))
            return projectService.updateData(projectDTO).toDTO();
        throw new IllegalSessionException();

    }

    @Override
    public void deleteProject(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) {

        if (!sessionService.isActual(sessionDTO)) throw new IllegalSessionException();
        projectService.delete(projectDTO.getId());
    }
}
