package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.parshin.tm.api.repository.ProjectRepository;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.IllegalNameException;
import ru.parshin.tm.exception.ProjectNotFoundException;
import ru.parshin.tm.exception.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@Transactional
@Component
public class ProjectServiceImpl extends AbstractService implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Override
    public Project select(@Nullable final String userId, @Nullable final String projectId) {

        if (projectId == null || projectId.isEmpty() || !isExists(projectId)) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();

        @NotNull Optional<Project> optionalProject = projectRepository.findByIdAndUser_id(projectId, userId);
        return optionalProject.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project create(@NotNull final User user, @Nullable final String name) {

        if (name == null || name.isEmpty()) throw new IllegalNameException();

        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);

        return projectRepository.save(project);
    }

    @Override
    public Iterable<Project> selectAll() {
        Iterable<Project> projects = projectRepository.findAll();
        return projects;
    }

    @NotNull
    @Override
    @Deprecated
    public List<Project> selectAll(@Nullable final String userId, @Nullable final String orderByColumn) {

        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();

        @NotNull Optional<List<Project>> optionalProject = Optional.ofNullable(projectRepository.findByUser_id(userId));
        return optionalProject.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project updateData(@Nullable final ProjectDTO projectDTO) {

        if (projectDTO == null || !isExists(projectDTO.getId())) throw new ProjectNotFoundException();

        final Optional<Project> toUpdate = projectRepository.findById(projectDTO.getId());
        final Project projectToUpdate = toUpdate.orElseThrow(ProjectNotFoundException::new);

        projectToUpdate.setName(projectDTO.getName());
        projectToUpdate.setDescription(projectDTO.getDescription());
        projectToUpdate.setDateBegin(projectDTO.getDateBegin());
        projectToUpdate.setDateEnd(projectDTO.getDateEnd());
        projectToUpdate.setStage(projectDTO.getStage());

        return projectRepository.save(projectToUpdate);
    }

    @Override
    public void delete(@Nullable final Project project) {

        if (project == null || !isExists(project.getId())) throw new ProjectNotFoundException();
        projectRepository.delete(project);
    }

    @Override
    public void delete(@Nullable final String id) {

        if (!isExists(id)) throw new ProjectNotFoundException();
        projectRepository.deleteById(id);
    }

    @Override
    public boolean isExists(@Nullable final String id) {

        if (id == null || id.isEmpty()) return false;

        return projectRepository.countById(id) > 0;

    }
}
