package ru.parshin.tm.exception;

public final class NullObjectException extends RuntimeException {
    public NullObjectException() {
        super("Передан пустой объект.");
    }
}
