package ru.parshin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

    @WebMethod
    TaskDTO createTask(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId")
            @Nullable final String projectId,
            @WebParam(name = "name")
            @Nullable final String name);

    @WebMethod
    TaskDTO readTask(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId")
            @Nullable final String projectId,
            @WebParam(name = "taskId")
            @Nullable final String taskId);

    @WebMethod
    @Nullable
    List<TaskDTO> readAllTasks(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId")
            @Nullable final String projectId,
            @WebParam(name = "orderBy")
            @Nullable final String orderBy);

    @WebMethod
    TaskDTO updateTask(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "taskDTO")
            @Nullable final TaskDTO taskDTO);

    @WebMethod
    void deleteTask(
            @WebParam(name = "sessionDTO")
            @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "taskDTO")
            @Nullable final TaskDTO taskDTO);
}
