package ru.parshin.tm.exception;

public class DeleteTaskException extends RuntimeException {
    public DeleteTaskException() {
        super("Ошибка при удалении задачи.");
    }
}
