package ru.parshin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.parshin.tm.api.endpoint.TaskEndpoint;
import ru.parshin.tm.api.service.ProjectService;
import ru.parshin.tm.api.service.SessionService;
import ru.parshin.tm.api.service.TaskService;
import ru.parshin.tm.api.service.UserService;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.dto.TaskDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.Task;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.IllegalSessionException;
import ru.parshin.tm.exception.TaskNotFoundException;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@WebService(endpointInterface = "ru.parshin.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private SessionService sessionService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;

    @Override
    public TaskDTO createTask(@Nullable final SessionDTO sessionDTO, @Nullable final String projectId, @Nullable final String name) {

        final User user = userService.select(sessionDTO.getUserId());
        final Project project = projectService.select(user.getId(), projectId);

        if (sessionService.isActual(sessionDTO))
            return taskService.create(user, project, name).toDTO();
        throw new IllegalSessionException();
    }

    @Override
    public TaskDTO readTask(@Nullable final SessionDTO sessionDTO, @Nullable final String projectId, @Nullable final String taskId) {

        if (sessionService.isActual(sessionDTO))
            return taskService.select(sessionDTO.getUserId(), projectId, taskId).toDTO();
        throw new IllegalSessionException();
    }

    @Override
    @Nullable
    public List<TaskDTO> readAllTasks(@Nullable final SessionDTO sessionDTO, @Nullable final String projectId, @Nullable final String orderBy) {

        if (sessionService.isActual(sessionDTO))
            return taskService
                    .selectAll(sessionDTO.getUserId(), projectId, orderBy)
                    .stream().map(Task::toDTO)
                    .collect(Collectors.toList());
        throw new IllegalSessionException();
    }

    @Override
    public TaskDTO updateTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) {

        if (sessionService.isActual(sessionDTO))
            return taskService.updateData(taskDTO).toDTO();
        throw new IllegalSessionException();
    }

    @Override
    public void deleteTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) {

        if (taskDTO == null) throw new TaskNotFoundException();
        if (!sessionService.isActual(sessionDTO)) throw new IllegalSessionException();
        taskService.delete(taskDTO.getId());
    }
}
