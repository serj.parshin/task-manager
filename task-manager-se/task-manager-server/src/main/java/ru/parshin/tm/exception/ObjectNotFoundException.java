package ru.parshin.tm.exception;

public final class ObjectNotFoundException extends RuntimeException {
    public ObjectNotFoundException() {
        super("Обьект не найден.");
    }
    public ObjectNotFoundException(String id) {
        super("Обьект с id: " + id + " не найден.");
    }
}
