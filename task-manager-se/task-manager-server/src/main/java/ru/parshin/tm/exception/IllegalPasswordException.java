package ru.parshin.tm.exception;

public final class IllegalPasswordException extends RuntimeException {
    public IllegalPasswordException() {
        super("Не верный пароль.");
    }
}
