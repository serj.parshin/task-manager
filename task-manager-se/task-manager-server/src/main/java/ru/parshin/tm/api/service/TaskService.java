package ru.parshin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.TaskDTO;
import ru.parshin.tm.domain.entity.Project;
import ru.parshin.tm.domain.entity.Task;
import ru.parshin.tm.domain.entity.User;

import java.util.List;

public interface TaskService {

    @NotNull Task create(@NotNull final User user, @NotNull final Project project, @Nullable final String name);

    @NotNull Task select(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

    @NotNull Task updateData(@Nullable final TaskDTO taskDTO);

    Iterable<Task> selectAll();

    @NotNull List<Task> selectAll(@Nullable final String userId, @Nullable final String projectId, @Nullable final String orderBy);

    void delete(@Nullable final String id);

    boolean isExists(@Nullable final String id);
}
