package ru.parshin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.parshin.tm.api.endpoint.UserEndpoint;
import ru.parshin.tm.api.service.SessionService;
import ru.parshin.tm.api.service.UserService;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.dto.UserDTO;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.IllegalSessionException;
import ru.parshin.tm.exception.NullObjectException;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@WebService(endpointInterface = "ru.parshin.tm.api.endpoint.UserEndpoint")
public class UserEndpointImpl implements UserEndpoint {

    @Autowired
    private UserService userService;
    @Autowired
    private SessionService sessionService;

    @Override
    public UserDTO registration(@Nullable final String login, @Nullable final String password) {

        return userService.registration(login, password).toDTO();
    }

    @Override
    public UserDTO readUser(@Nullable final SessionDTO sessionDTO, @Nullable final String userId) {

        if (sessionService.isActual(sessionDTO)) return userService.select(userId).toDTO();
        throw new IllegalSessionException();
    }

    @Override
    public List<UserDTO> readAllUsers(@Nullable final SessionDTO sessionDTO) {


        if (sessionService.isActual(sessionDTO)) return userService.isAdmin(sessionDTO.getUserId()) ?
                StreamSupport.stream(userService.selectAll().spliterator(), false).map(User::toDTO).collect(Collectors.toList())
                : null;
        throw new IllegalSessionException();
    }

    @Override
    public UserDTO updateUser(@Nullable final SessionDTO sessionDTO, @Nullable final UserDTO userDTO) {

        if (sessionService.isActual(sessionDTO)) return userService.updateData(userDTO).toDTO();
        throw new IllegalSessionException();

    }

    @Override
    public UserDTO updatePassword(@Nullable final SessionDTO sessionDTO, @Nullable final String password) {

        if (password == null) throw new NullObjectException();
        if (sessionService.isActual(sessionDTO))
            return userService.updatePassword(sessionDTO.getUserId(), password).toDTO();
        throw new IllegalSessionException();
    }

    @Override
    public void deleteUser(@Nullable final SessionDTO sessionDTO, @Nullable final UserDTO userDTO) {

        if (userDTO == null) return;
        if (!sessionService.isActual(sessionDTO)) throw new IllegalSessionException();
        userService.delete(userDTO.getId());
    }
}
