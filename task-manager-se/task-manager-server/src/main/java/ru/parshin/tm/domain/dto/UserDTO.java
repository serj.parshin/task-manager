package ru.parshin.tm.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.embedded.Name;
import ru.parshin.tm.domain.embedded.Role;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EqualsAndHashCode(of = "id", callSuper = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDTO extends BasedDTO {

    @Nullable
    @Column(unique = true)
    private String login;

    @Nullable
    @Column(unique = true)
    private String email;

    @Nullable
    private String passwordHash;

    @Nullable
    private Name name;

    @Nullable
    @Column(unique = true)
    private String phone;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role;

    private boolean locked;
}
