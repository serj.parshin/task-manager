package ru.parshin.tm.exception.service.session;

public class CreateSessionException extends RuntimeException {
    public CreateSessionException() {
        super("Ошибка при создании сессии.");
    }
}
