package ru.parshin.tm.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.embedded.Stage;
import ru.parshin.tm.util.adapter.ZonedDateTimeXmlAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EqualsAndHashCode(of = "id", callSuper = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectDTO extends BasedDTO implements Serializable {

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Stage stage;

    @Nullable
    @XmlJavaTypeAdapter(ZonedDateTimeXmlAdapter.class)
    private ZonedDateTime dateBegin;

    @Nullable
    @XmlJavaTypeAdapter(ZonedDateTimeXmlAdapter.class)
    private ZonedDateTime dateEnd;
}
