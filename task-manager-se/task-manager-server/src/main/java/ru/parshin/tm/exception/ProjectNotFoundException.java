package ru.parshin.tm.exception;

public final class ProjectNotFoundException extends RuntimeException {
    public ProjectNotFoundException() {
        super("Проект не найден.");
    }
}
