package ru.parshin.tm;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.parshin.tm.configuration.ApplicationConfiguration;
import ru.parshin.tm.context.ServerContext;

public final class Application {

    public static void main(String[] args) {

        final AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        context.getBean(ServerContext.class).run();
    }
}
