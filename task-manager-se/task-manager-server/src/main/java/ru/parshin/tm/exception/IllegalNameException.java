package ru.parshin.tm.exception;

public class IllegalNameException extends RuntimeException {
    public IllegalNameException() {
        super("Имя не может быть пустым!");
    }

    public IllegalNameException(String message) {
        super("Имя: " + message + " не может быть использовано.");
    }
}
