package ru.parshin.tm.util.encoder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Random;

public final class Signature {

    private final static String SALT = PasswordHash.md5(new Random().toString());
    private final static Integer CYCLE = 10;

    @Nullable
    public static String sign(@Nullable final Object value) {

        try {
            final long start = System.nanoTime();

            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            @Nullable final String result = sign(json, SALT, CYCLE);

            final long finish = System.nanoTime();
            final long delta = finish - start;
            final String time = delta / 1000 / 1000 + " ms";

            System.out.println("---------- SIGN : " + time);

            return result;

        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    private static String sign(@Nullable final String value, @Nullable final String salt, @Nullable final Integer cycle) {

        if (value == null || salt == null || cycle == null) return null;

        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = PasswordHash.md5(salt + result + salt);
        }

        return result;
    }
}