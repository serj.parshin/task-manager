package ru.parshin.tm.exception;

public class IllegalIdException extends RuntimeException {
    public IllegalIdException() {
        super("ID обьекта не может быть пустым!");
    }
}
