package ru.parshin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.parshin.tm.api.endpoint.SystemEndpoint;
import ru.parshin.tm.context.ServerSettings;

import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.parshin.tm.api.endpoint.SystemEndpoint")
public class SystemEndpointImpl implements SystemEndpoint {

    @Autowired
    private ServerSettings serverSettings;

    @Override
    public ServerSettings serverInfo() {
        return serverSettings;
    }
}
