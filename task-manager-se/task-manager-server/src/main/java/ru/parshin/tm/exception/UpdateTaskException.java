package ru.parshin.tm.exception;

public class UpdateTaskException extends RuntimeException {
    public UpdateTaskException() {
        super("Ошибка при обновлении задачи.");
    }
}
