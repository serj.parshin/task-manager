package ru.parshin.tm.exception;

public class CreateUserException extends RuntimeException {
    public CreateUserException() {
        super("При создании пользователя возникла ошибка.");
    }
}
