package ru.parshin.tm.exception;

public class DeleteEntityException extends RuntimeException {
    public DeleteEntityException() {
        super("Ошибка во время удаления сущности.");
    }
}
