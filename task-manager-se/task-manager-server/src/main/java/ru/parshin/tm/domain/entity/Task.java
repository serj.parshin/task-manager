package ru.parshin.tm.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.TaskDTO;
import ru.parshin.tm.domain.embedded.Stage;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EqualsAndHashCode(of = "id", callSuper = true)
public class Task extends AbstractEntity {

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Stage stage;

    @Nullable
    private ZonedDateTime dateBegin;

    @Nullable
    private ZonedDateTime dateEnd;

    @Nullable
    public TaskDTO toDTO() {
        try {
            final TaskDTO dto = new TaskDTO();
            dto.setId(getId());
            dto.setDateCreate(getDateCreate());
            dto.setUserId(getUser().getId());
            dto.setProjectId(getProject().getId());
            dto.setName(getName());
            dto.setDescription(getDescription());
            dto.setDateBegin(getDateBegin());
            dto.setDateEnd(getDateEnd());
            dto.setStage(getStage());
            return dto;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
