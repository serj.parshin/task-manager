package ru.parshin.tm.exception;

public final class EmptyStringException extends RuntimeException {
    public EmptyStringException() {
        super("Строка не может быть пустой");
    }
}
