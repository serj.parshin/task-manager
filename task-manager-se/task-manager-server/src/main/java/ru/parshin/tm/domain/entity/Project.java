package ru.parshin.tm.domain.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.domain.dto.ProjectDTO;
import ru.parshin.tm.domain.embedded.Stage;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EqualsAndHashCode(of = "id", callSuper = true)
public class Project extends AbstractEntity {

    @Nullable
    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Stage stage;

    @Nullable
    private ZonedDateTime dateBegin;

    @Nullable
    private ZonedDateTime dateEnd;

    @Nullable
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", orphanRemoval = true)
    private List<Task> tasks;

    @Nullable
    public ProjectDTO toDTO(){
        try {
            final ProjectDTO dto = new ProjectDTO();
            dto.setId(getId());
            dto.setDateCreate(getDateCreate());
            dto.setUserId(getUser().getId());
            dto.setName(getName());
            dto.setDescription(getDescription());
            dto.setDateBegin(getDateBegin());
            dto.setDateEnd(getDateEnd());
            dto.setStage(getStage());
            return dto;
        } catch (NullPointerException e){
            return null;
        }
    }
}
