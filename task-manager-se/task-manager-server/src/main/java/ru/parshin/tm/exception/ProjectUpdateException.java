package ru.parshin.tm.exception;

public class ProjectUpdateException extends RuntimeException {
    public ProjectUpdateException() {
        super("Ошибка при обновлении приекта.");
    }
}
