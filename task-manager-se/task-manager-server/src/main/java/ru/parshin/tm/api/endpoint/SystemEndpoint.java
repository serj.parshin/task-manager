package ru.parshin.tm.api.endpoint;

import ru.parshin.tm.context.ServerSettings;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface SystemEndpoint {

    @WebMethod
    ServerSettings serverInfo();
}
