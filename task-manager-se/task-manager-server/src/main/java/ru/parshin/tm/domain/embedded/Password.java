package ru.parshin.tm.domain.embedded;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.util.encoder.PasswordHash;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
@NoArgsConstructor
@EqualsAndHashCode(of = "hash")
public final class Password implements Serializable {

    @Nullable
    @Column(name = "passwordHash")
    private String hash;

    public Password(@NotNull final String ofString) {
        this.hash = PasswordHash.md5(ofString);
    }

    public void setHash(@NotNull final String hash) {
        this.hash = hash;
    }

    @Nullable
    public String getHash() {
        return hash;
    }

    @NotNull
    public Boolean isEqualsTo(@Nullable final String password) {
        if (password == null || password.isEmpty()) return false;
        if (hash == null || hash.isEmpty()) return false;
        return hash.equalsIgnoreCase(PasswordHash.md5(password));
    }

    @Override
    public String toString() {
        return getHash();
    }
}