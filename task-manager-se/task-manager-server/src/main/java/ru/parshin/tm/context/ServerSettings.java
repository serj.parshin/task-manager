package ru.parshin.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@Component
public class ServerSettings implements Serializable {

    private String serverHost;
    private String serverPort;

    public ServerSettings() {

        this.serverPort = System.getProperty("server.port");
        this.serverHost = System.getProperty("server.host");

        if (serverHost == null || serverHost.isEmpty() || serverHost.equals("null"))
            this.serverHost = "http://localhost:";
        else serverHost = "http://" + serverHost + ":";
        if (serverPort == null || serverPort.isEmpty() || serverPort.equals("null")) this.serverPort = "8080";
    }
}
