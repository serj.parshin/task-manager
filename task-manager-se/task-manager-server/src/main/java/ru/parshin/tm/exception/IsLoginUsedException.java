package ru.parshin.tm.exception;

public final class IsLoginUsedException extends RuntimeException {
    public IsLoginUsedException(String login) {
        super("Пользователь с логином " + login + " уже существует");
    }
}
