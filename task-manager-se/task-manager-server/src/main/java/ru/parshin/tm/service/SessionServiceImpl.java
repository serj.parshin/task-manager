package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.parshin.tm.api.repository.SessionRepository;
import ru.parshin.tm.api.service.SessionService;
import ru.parshin.tm.domain.dto.SessionDTO;
import ru.parshin.tm.domain.entity.Session;
import ru.parshin.tm.domain.entity.User;
import ru.parshin.tm.exception.SessionNotFoundException;
import ru.parshin.tm.exception.UserNotFoundException;

import java.util.Objects;
import java.util.Optional;

@Transactional
@Component
public class SessionServiceImpl extends AbstractService implements SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    @Override
    @NotNull
    public Session begin(@Nullable final User user) {

        if (user == null) throw new UserNotFoundException();

        @NotNull final Session session = new Session(user);
        session.sign();

        return sessionRepository.save(session);
    }

    @Nullable
    @Override
    public Session select(@Nullable final String userId) {

        if (userId == null) throw new UserNotFoundException();

        @NotNull final Optional<Session> optionalSessionDTO = sessionRepository.findByUser_id(userId);

        return optionalSessionDTO.orElseThrow(SessionNotFoundException::new);
    }

    @Override
    public void terminate(@Nullable final String sessionId) {

        if (!isExists(sessionId)) return;

        sessionRepository.deleteById(sessionId);
    }

    @Override
    public boolean isActual(@Nullable final SessionDTO sessionDTO) {

        if (sessionDTO == null) return false;
        if (!isExists(sessionDTO.getId())) return false;
        if (sessionDTO.getUserId() == null || sessionDTO.getUserId().isEmpty()) return false;

        @NotNull final Optional<Session> optionalSession = sessionRepository.findById(sessionDTO.getId());

        return optionalSession
                .filter(savedSession -> Objects.equals(savedSession.getSignature(), sessionDTO.getSignature()))
                .isPresent();
    }

    private boolean isExists(@Nullable final String id) {

        if (id == null || id.isEmpty()) return false;
        return sessionRepository.countById(id) > 0;
    }
}
