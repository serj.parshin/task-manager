package ru.parshin.tm.exception;

public class DataBaseConnectionException extends RuntimeException{
    public DataBaseConnectionException() {
        super("Отсутствует соедиенение с базой данных.");
    }
}
