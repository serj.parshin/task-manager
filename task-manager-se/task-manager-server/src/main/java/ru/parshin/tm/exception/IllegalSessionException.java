package ru.parshin.tm.exception;

public class IllegalSessionException extends RuntimeException {
    public IllegalSessionException() {
        super("Сессия не действительна.");
    }
}
