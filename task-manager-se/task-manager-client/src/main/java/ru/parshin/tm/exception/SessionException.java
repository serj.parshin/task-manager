package ru.parshin.tm.exception;

public class SessionException extends RuntimeException {
    public SessionException() {
        super("Необходима авторизация.");
    }
}
