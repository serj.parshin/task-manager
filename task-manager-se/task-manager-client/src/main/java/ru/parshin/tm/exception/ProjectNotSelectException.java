package ru.parshin.tm.exception;

public class ProjectNotSelectException extends RuntimeException {
    public ProjectNotSelectException() {
        super("Не выбран проект.");
    }
}
