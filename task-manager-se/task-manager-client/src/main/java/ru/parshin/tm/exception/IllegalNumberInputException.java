package ru.parshin.tm.exception;

public final class IllegalNumberInputException extends RuntimeException {
    public IllegalNumberInputException() {
        super("Ошибка при вводе числа.");
    }
}
