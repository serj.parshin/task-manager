package ru.parshin.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.parshin.tm")
public class ApplicationConfiguration {
}
