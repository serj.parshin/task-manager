package ru.parshin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.service.TerminalService;

public final class HelpCommand extends AbstractCommand {

    private static final Role ACCESS_LEVEL = Role.GUEST;
    private static final String COMMAND = "help";
    private static final String DESCRIPTION = "Выводит список доступных команд.";

    public HelpCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();

        getServiceLocator()
                .getCommands()
                .values()
                .stream()
                .filter(command -> (command.getAccessLevel().ordinal() <= getServiceLocator().getCurrentUser().getRole().ordinal()))
                .sorted((x, y) -> x.getName().compareToIgnoreCase(y.getName()))
                .forEach(command -> terminalService.println("\t" + command.toString()));
    }
}
