package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.exception.ProjectNotSelectException;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

public final class TaskSelectCommand extends AbstractTaskCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "select";
    private static final String DESCRIPTION = "Выбрать задачу.";

    public TaskSelectCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final TaskEndpoint taskEndpoint = getServiceLocator().getTaskEndpoint();
        @Nullable final ProjectDTO project = getServiceLocator().getSelectedProject();

        if (project == null) throw new ProjectNotSelectException();

        @Nullable final List<TaskDTO> tasks = taskEndpoint.readAllTasks(session, project.getId(), getServiceLocator().getTaskSortingField());
        terminalService.showListOfTasks(tasks);

        if (tasks == null || tasks.isEmpty()) return;

        @NotNull final Integer number = terminalService.readNumber();
        @NotNull final TaskDTO task = tasks.get(number);

        getServiceLocator().setSelectedTask(task);
    }
}
