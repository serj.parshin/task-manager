package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.exception.TaskNotSelectException;
import ru.parshin.tm.service.TerminalService;

public class TaskSetStageCommand extends AbstractTaskCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "set-stage";
    private static final String DESCRIPTION = "Изменить стадию задачи.";

    public TaskSetStageCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final TaskEndpoint taskEndpoint = getServiceLocator().getTaskEndpoint();
        @Nullable final TaskDTO task = getServiceLocator().getSelectedTask();
        @NotNull final Stage stage = terminalService.readStage();

        if (task == null) throw new TaskNotSelectException();

        task.setStage(stage);
        taskEndpoint.updateTask(session, task);
    }
}