package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.command.AbstractCommand;

abstract public class AbstractUserCommand extends AbstractCommand {

    private static final String PREFIX = "user-";

    AbstractUserCommand(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Role accessLevel,
            @NotNull final ServiceLocator serviceLocator) {
        super(PREFIX + name, description, accessLevel, serviceLocator);
    }


}
