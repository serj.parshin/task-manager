package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.ProjectDTO;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "list";
    private static final String DESCRIPTION = "Выводит список всех проектов.";

    public ProjectListCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final ProjectEndpoint projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull final List<ProjectDTO> projectList = projectEndpoint.readAllProjects(session, getServiceLocator().getProjectSortingField());

        terminalService.showListOfProjects(projectList);
    }
}
