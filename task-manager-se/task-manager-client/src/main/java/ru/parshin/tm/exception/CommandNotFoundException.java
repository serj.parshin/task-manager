package ru.parshin.tm.exception;

public final class CommandNotFoundException extends RuntimeException {
    public CommandNotFoundException(String command) {
        super("[Error]: '" + command + "' не существует\n" +
                "[Info]: Чтобы узнать список доступных команд введите: help");
    }
}
