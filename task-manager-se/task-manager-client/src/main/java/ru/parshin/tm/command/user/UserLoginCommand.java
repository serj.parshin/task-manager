package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.exception.SessionException;
import ru.parshin.tm.service.TerminalService;

public final class UserLoginCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.GUEST;
    private static final String COMMAND = "login";
    private static final String DESCRIPTION = "Авторизоваться в системе.";

    public UserLoginCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final SessionEndpoint sessionEndpoint = getServiceLocator().getSessionEndpoint();
        @NotNull final UserEndpoint userEndpoint = getServiceLocator().getUserEndpoint();

        @NotNull final String login = terminalService.readLogin();
        @NotNull final String password = terminalService.readPassword();

        @Nullable SessionDTO sessionDTO = getServiceLocator().getSession();
        sessionDTO = sessionEndpoint.beginSession(sessionDTO, login, password);

        if (sessionDTO == null) throw new SessionException();

        @Nullable final UserDTO userDTO = userEndpoint.readUser(sessionDTO, sessionDTO.getUserId());

        getServiceLocator().setSession(sessionDTO);
        getServiceLocator().setCurrentUser(userDTO);
    }
}
