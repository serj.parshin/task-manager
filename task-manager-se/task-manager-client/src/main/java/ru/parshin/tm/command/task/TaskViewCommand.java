package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.TaskDTO;
import ru.parshin.tm.exception.TaskNotSelectException;
import ru.parshin.tm.service.TerminalService;

public final class TaskViewCommand extends AbstractTaskCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "view";
    private static final String DESCRIPTION = "Показать содержимое задачи.";

    public TaskViewCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        final @NotNull TerminalService terminalService = getServiceLocator().getTerminalService();
        final @Nullable TaskDTO task = getServiceLocator().getSelectedTask();

        if (task == null) throw new TaskNotSelectException();

        terminalService.taskDetails(task);
    }
}
