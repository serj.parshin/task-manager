package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.ProjectDTO;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.TaskEndpoint;
import ru.parshin.tm.exception.ProjectNotSelectException;
import ru.parshin.tm.service.TerminalService;

public final class TaskCreateCommand extends AbstractTaskCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "create";
    private static final String DESCRIPTION = "Создать новую задачу.";

    public TaskCreateCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final TaskEndpoint taskEndpoint = getServiceLocator().getTaskEndpoint();
        @Nullable final ProjectDTO project = getServiceLocator().getSelectedProject();

        if (project == null) throw new ProjectNotSelectException();

        @NotNull final String name = terminalService.readName();
        taskEndpoint.createTask(session, name, project.getId());
    }
}