package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.ProjectDTO;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.exception.ProjectNotSelectException;

public final class ProjectDeleteCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "remove";
    private static final String DESCRIPTION = "Удалить проект.";

    public ProjectDeleteCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = getServiceLocator().getProjectEndpoint();
        @Nullable final ProjectDTO project = getServiceLocator().getSelectedProject();

        if (project == null) throw new ProjectNotSelectException();

        projectEndpoint.deleteProject(session, project);
        getServiceLocator().resetSelectedProject();
        getServiceLocator().resetSelectedTask();
    }
}
