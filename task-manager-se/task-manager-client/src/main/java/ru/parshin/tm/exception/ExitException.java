package ru.parshin.tm.exception;

public final class ExitException extends RuntimeException {
    public ExitException() {
        super("Завершение работы программы.");
    }
}
