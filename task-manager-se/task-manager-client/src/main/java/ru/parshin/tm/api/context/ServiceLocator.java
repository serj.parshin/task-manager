package ru.parshin.tm.api.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.service.TerminalService;

import java.util.Map;

public interface ServiceLocator {

    @NotNull SessionEndpoint getSessionEndpoint();
    @NotNull UserEndpoint getUserEndpoint();
    @NotNull ProjectEndpoint getProjectEndpoint();
    @NotNull TaskEndpoint getTaskEndpoint();
    @NotNull SystemEndpoint getSystemEndpoint();
    @NotNull TerminalService getTerminalService();

    @NotNull Map<String, AbstractCommand> getCommands();

    @Nullable SessionDTO getSession();
    void setSession(@Nullable final SessionDTO session);
    void resetSession();

    @NotNull UserDTO getCurrentUser();
    void setCurrentUser(@Nullable final UserDTO user);
    void resetCurrentUser();

    @NotNull UserDTO getSelectedUser();
    void setSelectedUser(@NotNull final UserDTO user);
    void resetSelectedUser();

    @Nullable ProjectDTO getSelectedProject();
    void setSelectedProject(@Nullable final ProjectDTO project);
    void resetSelectedProject();

    @Nullable TaskDTO getSelectedTask();
    void setSelectedTask(@Nullable final TaskDTO task);
    void resetSelectedTask();

    @Nullable String getUserSortingField();
    void setUserSortingField(@NotNull final String field);
    @Nullable String getProjectSortingField();
    void setProjectSortingField(@NotNull final String field);
    @Nullable String getTaskSortingField();
    void setTaskSortingField(@NotNull final String field);
}
