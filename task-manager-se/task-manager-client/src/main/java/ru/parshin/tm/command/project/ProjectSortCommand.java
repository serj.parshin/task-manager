package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.ProjectDTO;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

import static ru.parshin.tm.util.SortingMethodsInitiator.initSortingFields;

public class ProjectSortCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "sort";
    private static final String DESCRIPTION = "Отсортировать проекты.";

    public ProjectSortCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final ProjectEndpoint projectEndpoint =getServiceLocator().getProjectEndpoint();

        @NotNull final List<String> sortingFieldsList = initSortingFields(ProjectDTO.class);
        terminalService.showList(sortingFieldsList, "способов сортировки");

        if (sortingFieldsList.isEmpty()) return;

        @NotNull final Integer number = terminalService.readNumber();
        @NotNull final String sortingField = sortingFieldsList.get(number);

        getServiceLocator().setProjectSortingField(sortingField);

        @Nullable final List<ProjectDTO> projects = projectEndpoint.readAllProjects(session, sortingField);
        terminalService.showListOfProjects(projects);
    }
}
