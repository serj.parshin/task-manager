package ru.parshin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.exception.IllegalNumberInputException;

@UtilityClass
public final class InputValidator {

    public static int validateNumberInput(@Nullable final String input) {
        if (input == null || input.isEmpty()) throw new IllegalNumberInputException();
        return validateItemNumber(input);
    }

    private static int validateItemNumber(@NotNull final String input) {
        int out = Integer.parseInt(input);
        if (out < 0) {
            out = 0;
        } else {
            out--;
        }
        return out;
    }
}
