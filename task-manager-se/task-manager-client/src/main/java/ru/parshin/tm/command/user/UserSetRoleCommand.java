package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.UserDTO;
import ru.parshin.tm.api.endpoint.UserEndpoint;
import ru.parshin.tm.service.TerminalService;

public final class UserSetRoleCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.ADMIN;
    private static final String COMMAND = "set-role";
    private static final String DESCRIPTION = "Обновить роль.";

    public UserSetRoleCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final UserEndpoint userEndpoint = getServiceLocator().getUserEndpoint();
        @NotNull final UserDTO user = getServiceLocator().getSelectedUser();
        @NotNull final Role newRole = terminalService.readRole();

        user.setRole(newRole);

        userEndpoint.updateUser(session, user);
    }
}