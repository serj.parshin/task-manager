package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.command.AbstractCommand;

abstract class AbstractTaskCommand extends AbstractCommand {

    private static final String PREFIX = "task-";

    AbstractTaskCommand(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Role accessLevel,
            @NotNull final ServiceLocator serviceLocator) {
        super(PREFIX + name, description, accessLevel, serviceLocator);
    }
}
