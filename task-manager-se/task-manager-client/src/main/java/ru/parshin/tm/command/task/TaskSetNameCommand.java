package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.TaskDTO;
import ru.parshin.tm.api.endpoint.TaskEndpoint;
import ru.parshin.tm.exception.TaskNotSelectException;
import ru.parshin.tm.service.TerminalService;

final public class TaskSetNameCommand extends AbstractTaskCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "set-name";
    private static final String DESCRIPTION = "Переименовать задачу.";

    public TaskSetNameCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final TaskEndpoint taskEndpoint = getServiceLocator().getTaskEndpoint();
        @Nullable final TaskDTO task = getServiceLocator().getSelectedTask();

        if (task == null) throw new TaskNotSelectException();

        @NotNull final String name = terminalService.readName();
        task.setName(name);
        taskEndpoint.updateTask(session, task);
    }
}
