package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.ProjectDTO;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.exception.ProjectNotSelectException;
import ru.parshin.tm.service.TerminalService;

public final class ProjectSetNameCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "set-name";
    private static final String DESCRIPTION = "Изменить имя проекта.";

    public ProjectSetNameCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @Nullable final ProjectDTO project = getServiceLocator().getSelectedProject();
        @NotNull final String name = terminalService.readName();

        if (project == null) throw new ProjectNotSelectException();

        project.setName(name);
        projectEndpoint.updateProject(session, project);
    }
}
