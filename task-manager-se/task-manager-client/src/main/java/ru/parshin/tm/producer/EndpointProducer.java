package ru.parshin.tm.producer;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.endpoint.*;

@Component
public class EndpointProducer {
    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();
    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();
    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImplService().getProjectEndpointImplPort();
    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImplService().getTaskEndpointImplPort();
    @NotNull
    private final SystemEndpoint systemEndpoint = new SystemEndpointImplService().getSystemEndpointImplPort();

    @NotNull
    @Bean
    public ProjectEndpoint getProjectEndpoint() {
        return this.projectEndpoint;
    }

    @NotNull
    @Bean
    public SessionEndpoint getSessionEndpoint() {
        return this.sessionEndpoint;
    }

    @NotNull
    @Bean
    public SystemEndpoint getSystemEndpoint() {
        return this.systemEndpoint;
    }

    @NotNull
    @Bean
    public TaskEndpoint getTaskEndpoint() {
        return this.taskEndpoint;
    }

    @NotNull
    @Bean
    public UserEndpoint getUserEndpoint() {
        return this.userEndpoint;
    }
}
