package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.exception.ProjectNotSelectException;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

public final class ProjectViewCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "view";
    private static final String DESCRIPTION = "Показать содержимое проекта.";

    public ProjectViewCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final TaskEndpoint taskEndpoint = getServiceLocator().getTaskEndpoint();
        @Nullable final ProjectDTO project = getServiceLocator().getSelectedProject();

        if (project == null) throw new ProjectNotSelectException();

        @Nullable final List<TaskDTO> tasksOfProjectDTO = taskEndpoint.readAllTasks(session, project.getId(), getServiceLocator().getProjectSortingField());
        terminalService.projectDetails(project, tasksOfProjectDTO);
    }
}
