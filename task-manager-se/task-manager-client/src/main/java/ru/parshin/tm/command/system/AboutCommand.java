package ru.parshin.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.service.TerminalService;

public final class AboutCommand extends AbstractCommand {
    private static final Role ACCESS_LEVEL = Role.GUEST;
    private static final String COMMAND = "about";
    private static final String DESCRIPTION = "Выводит информацию о приложении.";

    public AboutCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();

        terminalService.println("[About]");
        terminalService.println(Manifests.read("Developer-Name"));
        terminalService.println(Manifests.read("Version-Info"));
        terminalService.println(Manifests.read("Build-Number"));
    }
}
