package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.service.TerminalService;

public final class UserViewCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "view";
    private static final String DESCRIPTION = "Показать профиль пользователя.";

    public UserViewCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        final @NotNull TerminalService terminalService = getServiceLocator().getTerminalService();
        terminalService.showUser(getServiceLocator().getCurrentUser());
    }
}
