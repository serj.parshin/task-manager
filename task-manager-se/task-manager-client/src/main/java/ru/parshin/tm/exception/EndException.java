package ru.parshin.tm.exception;

public final class EndException extends RuntimeException  {
    public EndException() {
        super("Редактирование завершено.");
    }
}
