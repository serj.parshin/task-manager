package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.exception.ProjectNotSelectException;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

import static ru.parshin.tm.util.SortingMethodsInitiator.initSortingFields;

public class TaskSortCommand extends AbstractTaskCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "sort";
    private static final String DESCRIPTION = "Отсортировать задачи.";

    public TaskSortCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final TaskEndpoint taskEndpoint = getServiceLocator().getTaskEndpoint();
        @Nullable final ProjectDTO project = getServiceLocator().getSelectedProject();

        if (project == null) throw new ProjectNotSelectException();

        @NotNull final List<String> sortingFieldsList = initSortingFields(TaskDTO.class);
        terminalService.showList(sortingFieldsList, "способов сортировки");

        if (sortingFieldsList.isEmpty()) return;

        @NotNull final Integer number = terminalService.readNumber();
        @NotNull final String sortingField = sortingFieldsList.get(number);

        getServiceLocator().setTaskSortingField(sortingField);

        @Nullable final List<TaskDTO> tasks = taskEndpoint.readAllTasks(session, project.getId(), sortingField);
        terminalService.showListOfTasks(tasks);
    }
}
