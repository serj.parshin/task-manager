package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.UserDTO;
import ru.parshin.tm.api.endpoint.UserEndpoint;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

public final class UserListCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.ADMIN;
    private static final String COMMAND = "list";
    private static final String DESCRIPTION = "Показать список пользователей.";

    public UserListCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        final @NotNull TerminalService terminalService = getServiceLocator().getTerminalService();
        final @NotNull UserEndpoint userEndpoint = getServiceLocator().getUserEndpoint();
        final @Nullable SessionDTO session = getServiceLocator().getSession();
        final @NotNull List<UserDTO> users = userEndpoint.readAllUsers(session);

        terminalService.showListOfUsers(users);
    }
}
