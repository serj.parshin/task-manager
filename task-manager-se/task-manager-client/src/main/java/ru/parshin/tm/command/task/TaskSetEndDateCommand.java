package ru.parshin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.TaskDTO;
import ru.parshin.tm.api.endpoint.TaskEndpoint;
import ru.parshin.tm.exception.TaskNotSelectException;
import ru.parshin.tm.service.TerminalService;

import java.time.ZonedDateTime;

public final class TaskSetEndDateCommand extends AbstractTaskCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "set-end-date";
    private static final String DESCRIPTION = "Изменить дату конца.";

    public TaskSetEndDateCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final TaskEndpoint taskEndpoint = getServiceLocator().getTaskEndpoint();
        @Nullable final TaskDTO task = getServiceLocator().getSelectedTask();

        if (task == null) throw new TaskNotSelectException();

        @NotNull final ZonedDateTime date = terminalService.readDate("конца");
        task.setDateEnd(date.toString());
        taskEndpoint.updateTask(session, task);
    }
}
