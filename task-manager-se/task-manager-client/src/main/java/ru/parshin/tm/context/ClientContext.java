package ru.parshin.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.command.project.*;
import ru.parshin.tm.command.system.AboutCommand;
import ru.parshin.tm.command.system.ExitCommand;
import ru.parshin.tm.command.system.HelpCommand;
import ru.parshin.tm.command.system.ServerInfoCommand;
import ru.parshin.tm.command.task.*;
import ru.parshin.tm.command.user.*;
import ru.parshin.tm.controller.CommandExecutor;
import ru.parshin.tm.exception.ExitException;
import ru.parshin.tm.service.TerminalService;
import ru.parshin.tm.util.CommandParser;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@Component
public class ClientContext implements Runnable, ServiceLocator {

    @Autowired
    private SessionEndpoint sessionEndpoint;
    @Autowired
    private UserEndpoint userEndpoint;
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private TaskEndpoint taskEndpoint;
    @Autowired
    private SystemEndpoint systemEndpoint;
    @Autowired
    private TerminalService terminalService;

    @NotNull
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    @Nullable
    private SessionDTO session = null;
    @Nullable
    private UserDTO currentUser = null;
    @Nullable
    private UserDTO selectedUser = null;
    @Nullable
    private ProjectDTO selectedProject = null;
    @Nullable
    private TaskDTO selectedTask = null;

    @Nullable
    private String userSortingField = null;
    @Nullable
    private String projectSortingField = null;
    @Nullable
    private String taskSortingField = null;

    @Override
    public void run() {

        @NotNull final CommandExecutor executor = new CommandExecutor(commands, this);

        terminalService.println(" *** Добро пожаловать в менеджер задач *** ");
        terminalService.println("Для завершения работы программы введите: exit");

        while (true) {
            try {
                @NotNull final String userInput = terminalService.readCommand();
                executor.execute(userInput);

            } catch (Exception e) {
                if (e.getClass().equals(ExitException.class)) return;
                terminalService.printError(e.getMessage());
                e.printStackTrace();        // заменить на логер
            }
        }
    }

    @PostConstruct
    public void initCommands() {

        @NotNull final Class[] COMMANDS = {
                AboutCommand.class, HelpCommand.class, ExitCommand.class,

                UserListCommand.class, UserLogoutCommand.class, UserLoginCommand.class,
                UserRegistrationCommand.class, UserSelectCommand.class, UserSetLoginCommand.class,
                UserSetNameCommand.class, UserSetPasswordCommand.class, UserSetRoleCommand.class,
                UserViewCommand.class,

                ProjectCreateCommand.class, ProjectDeleteCommand.class, ProjectListCommand.class,
                ProjectSelectCommand.class, ProjectSetEndDateCommand.class, ProjectSetStartDateCommand.class,
                ProjectSetNameCommand.class, ProjectViewCommand.class, ProjectSetStageCommand.class,

                TaskCreateCommand.class, TaskDeleteCommand.class, TaskListCommand.class,
                TaskSelectCommand.class, TaskSetDescriptionCommand.class, TaskSetEndDateCommand.class,
                TaskSetStartDateCommand.class, TaskSetNameCommand.class, TaskViewCommand.class,
                TaskSetStageCommand.class,

                ProjectSortCommand.class, TaskSortCommand.class,

                ServerInfoCommand.class,
        };

        CommandParser.parseCommands(COMMANDS, this);
    }

    @Override
    public void resetSession() {
        this.session = null;
        resetCurrentUser();
        resetSelectedUser();
        resetSelectedProject();
        resetSelectedTask();
    }

    @NotNull
    @Override
    public UserDTO getCurrentUser() {

        if (currentUser != null) return currentUser;

        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.GUEST);
        return user;
    }

    @Override
    public void resetCurrentUser() {
        this.currentUser = null;
    }

    @NotNull
    @Override
    public UserDTO getSelectedUser() {
        return selectedUser == null ? getCurrentUser() : selectedUser;
    }

    @Override
    public void resetSelectedUser() {
        this.selectedUser = null;
    }

    @Override
    public void resetSelectedProject() {
        this.selectedProject = null;
    }

    @Override
    public void resetSelectedTask() {
        this.selectedTask = null;
    }
}
