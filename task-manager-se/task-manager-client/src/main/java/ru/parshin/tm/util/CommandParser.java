package ru.parshin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.command.AbstractCommand;

import java.lang.reflect.Constructor;

@UtilityClass
public final class CommandParser {

    public static void parseCommands(@NotNull final Class[] classes, @NotNull final ServiceLocator serviceLocator) {

        try {
            for (@NotNull final Class<?> aClass : classes) {

                if (isAbstractCommand(aClass)) {
                    Constructor[] constructors = aClass.getConstructors();

                    for (Constructor constructor : constructors) {

                        AbstractCommand command = (AbstractCommand) constructor.newInstance(serviceLocator);
                        serviceLocator
                                .getCommands()
                                .put(command.getName(), command);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isAbstractCommand(@NotNull final Class aClass) {
        return (aClass.getSuperclass().equals(AbstractCommand.class) || aClass.getSuperclass().getSuperclass().equals(AbstractCommand.class));
    }
}
