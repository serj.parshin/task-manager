package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.UserDTO;
import ru.parshin.tm.api.endpoint.UserEndpoint;
import ru.parshin.tm.service.TerminalService;

public final class UserSetLoginCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "set-login";
    private static final String DESCRIPTION = "Обновить логин.";

    public UserSetLoginCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final UserEndpoint userEndpoint = getServiceLocator().getUserEndpoint();
        @NotNull final UserDTO user = getServiceLocator().getCurrentUser();

        terminalService.println("Текущий логин: " + user.getLogin());

        @NotNull final String newLogin = terminalService.readLogin("новый");
        user.setLogin(newLogin);
        userEndpoint.updateUser(session, user);

        user.setLogin(newLogin);
    }
}
