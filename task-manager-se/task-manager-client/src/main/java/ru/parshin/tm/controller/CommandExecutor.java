package ru.parshin.tm.controller;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.exception.CommandNotFoundException;
import ru.parshin.tm.service.TerminalService;

import java.util.Map;
import java.util.Optional;

public final class CommandExecutor {

    @NotNull
    private final Map<String, AbstractCommand> commands;
    @NotNull
    private final ServiceLocator serviceLocator;

    public CommandExecutor(@NotNull final Map<String, AbstractCommand> commands, @NotNull final ServiceLocator serviceLocator) {
        this.commands = commands;
        this.serviceLocator = serviceLocator;
    }

    public void execute(@NotNull final String userInput) throws Exception {

        @NotNull final AbstractCommand command = findCommandOrThrow(userInput);
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();

        terminalService.printCallToAction(command);
        command.execute();
        terminalService.printSuccess(command);
    }

    private AbstractCommand findCommandOrThrow(@NotNull final String userInput) {
        return Optional
                .ofNullable(commands.get(userInput))
                .orElseThrow(() -> new CommandNotFoundException(userInput));
    }
}
