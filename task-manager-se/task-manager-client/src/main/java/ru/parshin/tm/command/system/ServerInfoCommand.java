package ru.parshin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.ServerSettings;
import ru.parshin.tm.api.endpoint.SystemEndpoint;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.service.TerminalService;

public final class ServerInfoCommand extends AbstractCommand {
    private static final Role ACCESS_LEVEL = Role.GUEST;
    private static final String COMMAND = "server-info";
    private static final String DESCRIPTION = "Показать информацию о сервере.";

    public ServerInfoCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final SystemEndpoint systemEndpoint = getServiceLocator().getSystemEndpoint();

        final ServerSettings serverInfo = systemEndpoint.serverInfo();

        terminalService.showServerInfo(serverInfo);
    }
}
