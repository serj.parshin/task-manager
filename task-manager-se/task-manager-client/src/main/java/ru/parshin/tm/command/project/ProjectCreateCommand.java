package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.service.TerminalService;

public final class ProjectCreateCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "create";
    private static final String DESCRIPTION = "Создать новый проект.";

    public ProjectCreateCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final ProjectEndpoint projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull final String projectName = terminalService.readName();

        projectEndpoint.createProject(session, projectName);
    }
}
