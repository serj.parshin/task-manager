package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.UserDTO;
import ru.parshin.tm.api.endpoint.UserEndpoint;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

public final class UserSelectCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.ADMIN;
    private static final String COMMAND = "select";
    private static final String DESCRIPTION = "Выбрать пользователя.";

    public UserSelectCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final UserEndpoint userEndpoint = getServiceLocator().getUserEndpoint();

        @Nullable final List<UserDTO> users = userEndpoint.readAllUsers(session);
        terminalService.showListOfUsers(users);
        if (users == null || users.isEmpty()) return;

        @NotNull final Integer number = terminalService.readNumber();
        @NotNull final UserDTO user = users.get(number);

        getServiceLocator().setSelectedUser(user);
    }
}
