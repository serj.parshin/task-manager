package ru.parshin.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;

@Getter
public abstract class AbstractCommand {

    @NotNull
    private final String name;
    @NotNull
    private final String description;
    @NotNull
    private final Role accessLevel;
    @NotNull
    private final ServiceLocator serviceLocator;

    protected AbstractCommand(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Role accessLevel,
            @NotNull final ServiceLocator serviceLocator
    ) {
        this.name = name;
        this.description = description;
        this.accessLevel = accessLevel;
        this.serviceLocator = serviceLocator;
    }

    abstract public void execute() throws Exception;

    @NotNull
    public Role getAccessLevel() {
        return accessLevel;
    }

    @Override
    @NotNull
    public String toString() {
        return name + ": " + description;
    }
}
