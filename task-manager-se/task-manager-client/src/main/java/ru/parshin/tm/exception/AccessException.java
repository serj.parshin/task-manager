package ru.parshin.tm.exception;

public final class AccessException extends RuntimeException {
    public AccessException() {
        super("Не достаточно прав для исполнения команды.");
    }
}
