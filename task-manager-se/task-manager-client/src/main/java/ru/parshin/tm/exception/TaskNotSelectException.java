package ru.parshin.tm.exception;

public class TaskNotSelectException extends RuntimeException {
    public TaskNotSelectException() {
        super("Задача не выбрана.");
    }
}
