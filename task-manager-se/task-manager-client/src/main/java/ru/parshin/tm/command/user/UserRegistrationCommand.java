package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.UserEndpoint;
import ru.parshin.tm.service.TerminalService;

public final class UserRegistrationCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.GUEST;
    private static final String COMMAND = "registration";
    private static final String DESCRIPTION = "Зарегистрировать нового пользоватлея.";

    public UserRegistrationCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        final @NotNull UserEndpoint userEndpoint = getServiceLocator().getUserEndpoint();
        final @NotNull TerminalService terminalService = getServiceLocator().getTerminalService();
        final @NotNull String login = terminalService.readLogin();
        final @NotNull String password = terminalService.readPassword();

        userEndpoint.registration(login, password);
    }
}
