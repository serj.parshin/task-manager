package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.exception.ProjectNotSelectException;
import ru.parshin.tm.service.TerminalService;

public class ProjectSetStageCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "set-stage";
    private static final String DESCRIPTION = "Изменить стадию проекта.";

    public ProjectSetStageCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final ProjectEndpoint projectEndpoint = getServiceLocator().getProjectEndpoint();
        @Nullable final ProjectDTO project = getServiceLocator().getSelectedProject();
        @NotNull final Stage stage = terminalService.readStage();

        if (project == null) throw new ProjectNotSelectException();

        project.setStage(stage);
        projectEndpoint.updateProject(session, project);
    }
}