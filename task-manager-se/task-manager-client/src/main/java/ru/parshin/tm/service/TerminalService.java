package ru.parshin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.parshin.tm.api.endpoint.*;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.util.InputValidator;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.out;

@Component
public final class TerminalService extends AbstractService {

    @NotNull
    private static final Scanner input = new Scanner(System.in);
    @NotNull
    private static final String europeanDatePattern = "dd.MM.yyyy";
    @NotNull
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(europeanDatePattern);

    @NotNull
    private ZonedDateTime readFormatDate(@NotNull final String input) {
        @NotNull final LocalDate parsed = LocalDate.parse(input, dateTimeFormatter);
        return ZonedDateTime.of(parsed, LocalTime.MIDNIGHT, ZoneId.of("Europe/Moscow"));
    }

    @NotNull
    private String formatDate(@NotNull final String dateTime) {
        return ZonedDateTime.parse(dateTime).format(dateTimeFormatter);
    }

    @NotNull
    public String readCommand() {
        out.print("Введите команду: ");
        return input.nextLine();
    }

    @NotNull
    public Integer readNumber() {
        out.print("Введите номер: ");
        return InputValidator.validateNumberInput(input.nextLine());
    }

    @NotNull
    public String readName() {
        out.print("Введите имя: ");
        return input.nextLine();
    }

    @NotNull
    public String readLine() {
        out.print("Введите: ");
        return input.nextLine();
    }

    @NotNull
    public String readDescription() {
        out.print("Введите описание: ");
        return input.nextLine();
    }

    @NotNull
    public ZonedDateTime readDate(@NotNull final String type) {
        out.print("Введите дату " + type + " в формате DD.MM.YYYY: ");
        return readFormatDate(input.nextLine());
    }

    @NotNull
    public String readLogin() {
        out.print("Введите логин: ");
        return input.nextLine();
    }

    @NotNull
    public String readLogin(@NotNull final String type) {
        out.print("Введите " + type + " логин: ");
        return input.nextLine();
    }

    @NotNull
    public Role readRole() {
        out.println("Список доступных ролей: ");
        Arrays.stream(Role.values()).forEach(out::println);

        out.print("Введите роль для пользователя: ");
        final String userInput = input.nextLine();

        return Role.valueOf(userInput);
    }

    @NotNull
    public Stage readStage() {
        out.println("Список доступных стадий: ");
        for (Stage value : Stage.values()) {
            out.println("\t" + value.toString().toLowerCase());
        }
        out.print("Введите стадию: ");
        final String userInput = input.nextLine().toUpperCase();
        return Stage.valueOf(userInput);
    }

    @NotNull
    public String readPassword() {
        out.print("Введите пароль: ");
        return input.nextLine();
    }

    @NotNull
    public String readPassword(@NotNull final String type) {
        out.print("Введите " + type + " пароль: ");
        return input.nextLine();
    }

    public void println(@NotNull final String text) {
        out.println(text);
    }

    public void printSuccess(@NotNull final AbstractCommand command) {
        out.println("[ OK ]\t" + command.getName());
    }

    public void printError(@NotNull final String errorMessage) {
        out.println("[ Error ]\t" + errorMessage);
    }

    public void printCallToAction(AbstractCommand command) {
        out.println("[ " + command.getName() + " ] - " + command.getDescription());
    }

    public void showList(@Nullable final List<?> list, @NotNull final String type) {
        println("[Список " + type + "]: ");
        if (list == null || list.isEmpty()) {
            println("\tпуст.");
        } else {
            for (int i = 1; i <= list.size(); i++) {
                println("\t" + i + ". " + list.get(i - 1).toString());
            }
        }
    }

    public void showListOfUsers(@Nullable final List<UserDTO> list) {
        println("[Список польщователей]: ");
        if (list == null || list.isEmpty()) {
            println("\tпуст.");
        } else {
            for (int i = 1; i <= list.size(); i++) {
                @NotNull final UserDTO user = list.get(i - 1);
                println("\t" + i + ". " + user.getId()
                        + " login: " + user.getLogin());
            }
        }
    }

    public void showListOfProjects(@Nullable final List<ProjectDTO> list) {
        println("[Список проектов]: ");
        if (list == null || list.isEmpty()) {
            println("\tпуст.");
        } else {
            for (int i = 1; i <= list.size(); i++) {
                @NotNull final ProjectDTO project = list.get(i - 1);
                println("\t" + i + ". " + project.getName()
                        + " begin: " + formatDate(project.getDateBegin())
                        + " end: " + formatDate(project.getDateEnd()));
            }
        }
    }

    public void showListOfTasks(@Nullable final List<TaskDTO> list) {
        println("[Список задач]: ");
        if (list == null || list.isEmpty()) {
            println("\tпуст.");
        } else {
            for (int i = 1; i <= list.size(); i++) {
                @NotNull final TaskDTO task = list.get(i - 1);
                println("\t" + i + ". " + task.getName()
                        + " begin: " + formatDate(task.getDateBegin())
                        + " end: " + formatDate(task.getDateEnd()));
            }
        }
    }

    public void projectDetails(@NotNull final ProjectDTO project, @Nullable final List<TaskDTO> tasksOfProject) {
        println("\tИмя: " + project.getName());
        println("\tФаза: " + project.getStage());
        println("\tДата старта: " + ZonedDateTime.parse(project.getDateBegin()).toLocalDate().format(dateTimeFormatter));
        println("\tДата завершения: " + ZonedDateTime.parse(project.getDateEnd()).toLocalDate().format(dateTimeFormatter));
        showListOfTasks(tasksOfProject);
    }

    public void taskDetails(@NotNull final TaskDTO task) {
        println("\tИмя: " + task.getName());
        println("\tОписание: " + task.getDescription());
        println("\tФаза: " + task.getStage());
        println("\tДата старта: " + ZonedDateTime.parse(task.getDateBegin()).toLocalDate().format(dateTimeFormatter));
        println("\tДата завершения: " + ZonedDateTime.parse(task.getDateEnd()).toLocalDate().format(dateTimeFormatter));
    }

    public void showUser(@NotNull final UserDTO user) {
        println("\tID: " + user.getId());
        println("\tИмя: " + user.getName().getFirstName() + " " + user.getName().getLastName() + " " + user.getName().getMiddleName());
        println("\tLogin: " + user.getLogin());
        println("\tПрава: " + user.getRole());
    }

    public void showServerInfo(@NotNull final ServerSettings serverInfo) {
        println("Host: " + serverInfo.getServerHost());
        println("Port: " + serverInfo.getServerPort());
    }
}
