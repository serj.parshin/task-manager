package ru.parshin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.command.AbstractCommand;
import ru.parshin.tm.exception.ExitException;

public final class ExitCommand extends AbstractCommand {
    private static final Role ACCESS_LEVEL = Role.GUEST;
    private static final String COMMAND = "exit";
    private static final String DESCRIPTION = "Завершить работу программы.";

    public ExitCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {
        throw new ExitException();
    }
}
