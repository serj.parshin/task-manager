package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.UserEndpoint;
import ru.parshin.tm.service.TerminalService;

public final class UserSetPasswordCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "set-password";
    private static final String DESCRIPTION = "Обновить пароль.";

    public UserSetPasswordCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final UserEndpoint userEndpoint = getServiceLocator().getUserEndpoint();
        @NotNull final String newPassword = terminalService.readPassword();

        userEndpoint.updatePassword(session, newPassword);
    }
}
