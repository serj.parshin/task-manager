package ru.parshin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class SortingMethodsInitiator {
    public static List<String> initSortingFields(Class c) {
        @NotNull final Field[] fields = c.getDeclaredFields();
        return Arrays.stream(fields)
                .map(Field::getName)
                .filter(field -> !field.toLowerCase().contains("id"))
                .collect(Collectors.toList());
    }
}
