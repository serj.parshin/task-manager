package ru.parshin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.ProjectDTO;
import ru.parshin.tm.api.endpoint.ProjectEndpoint;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.service.TerminalService;

import java.util.List;

public final class ProjectSelectCommand extends AbstractProjectCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "select";
    private static final String DESCRIPTION = "Выбрать проект.";

    public ProjectSelectCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionDTO session = getServiceLocator().getSession();
        @NotNull final TerminalService terminalService = getServiceLocator().getTerminalService();
        @NotNull final ProjectEndpoint projectEndpoint = getServiceLocator().getProjectEndpoint();
        @Nullable final List<ProjectDTO> projectList = projectEndpoint.readAllProjects(session, getServiceLocator().getProjectSortingField());

        terminalService.showListOfProjects(projectList);
        if (projectList == null || projectList.isEmpty()) return;

        @NotNull final Integer number = terminalService.readNumber();
        @NotNull final ProjectDTO project = projectList.get(number);

        getServiceLocator().setSelectedProject(project);
    }
}
