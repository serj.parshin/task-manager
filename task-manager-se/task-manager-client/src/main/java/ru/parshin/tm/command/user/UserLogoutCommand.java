package ru.parshin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.parshin.tm.api.context.ServiceLocator;
import ru.parshin.tm.api.endpoint.Role;
import ru.parshin.tm.api.endpoint.SessionDTO;
import ru.parshin.tm.api.endpoint.SessionEndpoint;

public final class UserLogoutCommand extends AbstractUserCommand {
    private static final Role ACCESS_LEVEL = Role.USER;
    private static final String COMMAND = "logout";
    private static final String DESCRIPTION = "Завершить сеанс.";

    public UserLogoutCommand(@NotNull final ServiceLocator serviceLocator) {
        super(COMMAND, DESCRIPTION, ACCESS_LEVEL, serviceLocator);
    }

    @Override
    public void execute() {

        @NotNull final SessionEndpoint sessionEndpoint = getServiceLocator().getSessionEndpoint();
        @Nullable final SessionDTO session = getServiceLocator().getSession();

        sessionEndpoint.terminateSession(session);
        getServiceLocator().resetSession();
    }
}
