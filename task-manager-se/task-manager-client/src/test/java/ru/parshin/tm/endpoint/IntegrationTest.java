package ru.parshin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import ru.parshin.tm.api.endpoint.*;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

@Tag("integration-tests")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class IntegrationTest {

    @NotNull
    final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();
    @NotNull
    final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();
    @NotNull
    final ProjectEndpoint projectEndpoint = new ProjectEndpointImplService().getProjectEndpointImplPort();
    @NotNull
    final TaskEndpoint taskEndpoint = new TaskEndpointImplService().getTaskEndpointImplPort();

    @Nullable
    private SessionDTO sessionDTO = null;
    @Nullable
    private SessionDTO invalidSessionDTO = new SessionDTO();
    @Nullable
    private UserDTO userDTO = null;
    @Nullable
    private ProjectDTO projectDTO = null;
    @Nullable
    private TaskDTO taskDTO = null;

    @NotNull
    private String login = "integrationTest";
    @NotNull
    private String password = "integrationTest";

    @Test
    @Order(1)
    void registration() {

        userDTO = userEndpoint.registration(login, password);

        assertNotNull(userDTO);
        assertNotNull(userDTO.getId());
        assertNotNull(userDTO.getDateCreate());
        assertNotNull(userDTO.getLogin());
        assertNotNull(userDTO.getPasswordHash());
        assertEquals(login, userDTO.getLogin());
        assertNotEquals(password, userDTO.getPasswordHash());
    }

    @Test
    @Order(2)
    void beginSession() {

        sessionDTO = sessionEndpoint.beginSession(sessionDTO, login, password);

        invalidSessionDTO.setId(sessionDTO.getId());
        invalidSessionDTO.setDateCreate(sessionDTO.getDateCreate());
        invalidSessionDTO.setUserId(sessionDTO.getUserId());
        invalidSessionDTO.setSignature("invalid");

        assertNotNull(sessionDTO);
        assertNotNull(sessionDTO.getId());
        assertNotNull(sessionDTO.getDateCreate());
        assertNotNull(sessionDTO.getUserId());
        assertNotNull(sessionDTO.getSignature());

        assertEquals(userDTO.getId(), sessionDTO.getUserId());
    }

    @Test
    @Order(3)
    void readUser() {

        UserDTO readedUser = userEndpoint.readUser(sessionDTO, sessionDTO.getUserId());

        assertNotNull(readedUser);
        assertNotNull(readedUser.getId());
        assertNotNull(readedUser.getDateCreate());
        assertEquals(login, readedUser.getLogin());
        assertNotEquals(password, readedUser.getPasswordHash());

        assertEquals(sessionDTO.getUserId(), readedUser.getId());
    }

    @Test
    @Order(3)
    void readUserWihtInvalidSession() {

        assertThrows(Exception.class, () -> userEndpoint.readUser(invalidSessionDTO, invalidSessionDTO.getUserId()));

    }

    @Test
    @Order(4)
    void updateUser() {

        Name name = new Name();
        name.setFirstName("first");
        name.setLastName("last");
        name.setMiddleName("middle");
        String email = "test@email.test";
        String phone = "918273645";

        userDTO.setName(name);
        userDTO.setEmail(email);
        userDTO.setPhone(phone);

        userDTO = userEndpoint.updateUser(sessionDTO, userDTO);

        assertEquals(name.getFirstName(), userDTO.getName().getFirstName());
        assertEquals(name.getLastName(), userDTO.getName().getLastName());
        assertEquals(name.getMiddleName(), userDTO.getName().getMiddleName());
        assertEquals(email, userDTO.getEmail());
        assertEquals(phone, userDTO.getPhone());
    }

    @Test
    @Order(5)
    void updatePassword() {

        String newPassword = "testPassword";
        String oldHash = userDTO.getPasswordHash();

        userDTO = userEndpoint.updatePassword(sessionDTO, newPassword);

        assertNotEquals(oldHash, userDTO.getPasswordHash());
    }

    @Test
    @Order(6)
    void createProject() {

        String projectName = "integration tests project";
        projectDTO = projectEndpoint.createProject(sessionDTO, projectName);

        assertNotNull(projectDTO);
        assertNotNull(projectDTO.getId());
        assertNotNull(projectDTO.getDateCreate());
        assertNotNull(projectDTO.getUserId());
        assertNotNull(projectDTO.getName());
        assertEquals(userDTO.getId(), projectDTO.getUserId());
        assertEquals(projectName, projectDTO.getName());
        assertNull(projectDTO.getStage());
    }

    @Test
    @Order(7)
    void readProject() {

        projectDTO = projectEndpoint.readProject(sessionDTO, projectDTO.getId());

        assertNotNull(projectDTO);
        assertNotNull(projectDTO.getId());
        assertNotNull(projectDTO.getDateCreate());
        assertNotNull(projectDTO.getUserId());
        assertNotNull(projectDTO.getName());
        assertNull(projectDTO.getStage());
    }

    @Test
    @Order(8)
    void updateProject() {

        String name = "updated name";
        String description = "updated description";
        ZonedDateTime dateBegin = ZonedDateTime.now();
        ZonedDateTime dateEnd = ZonedDateTime.now();
        Stage stage = Stage.PROCESSING;

        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectDTO.setDateBegin(dateBegin.toString());
        projectDTO.setDateEnd(dateEnd.toString());
        projectDTO.setStage(stage);

        projectDTO = projectEndpoint.updateProject(sessionDTO, projectDTO);

        assertNotNull(projectDTO);
        assertNotNull(projectDTO.getId());
        assertNotNull(projectDTO.getDateCreate());
        assertNotNull(projectDTO.getUserId());
        assertNotNull(projectDTO.getName());
        assertNotNull(projectDTO.getDescription());
        assertNotNull(projectDTO.getDateBegin());
        assertNotNull(projectDTO.getDateEnd());
        assertNotNull(projectDTO.getStage());

        assertEquals(name, projectDTO.getName());
        assertEquals(description, projectDTO.getDescription());
        assertEquals(dateBegin.toString(), projectDTO.getDateBegin());
        assertEquals(dateEnd.toString(), projectDTO.getDateEnd());
        assertEquals(stage, projectDTO.getStage());
    }

    @Test
    @Order(9)
    void createTask() {

        String taskName = "integration tests task";
        taskDTO = taskEndpoint.createTask(sessionDTO, projectDTO.getId(), taskName);

        assertNotNull(taskDTO);
        assertNotNull(taskDTO.getId());
        assertNotNull(taskDTO.getDateCreate());
        assertNotNull(taskDTO.getUserId());
        assertNotNull(taskDTO.getProjectId());
        assertNotNull(taskDTO.getName());
        assertNull(taskDTO.getStage());

        assertEquals(userDTO.getId(), taskDTO.getUserId());
        assertEquals(projectDTO.getId(), taskDTO.getProjectId());
        assertEquals(taskName, taskDTO.getName());
        assertNull(taskDTO.getStage());
    }

    @Test
    @Order(10)
    void readTask() {

        taskDTO = taskEndpoint.readTask(sessionDTO, projectDTO.getId(), taskDTO.getId());

        assertNotNull(taskDTO);
        assertNotNull(taskDTO.getId());
        assertNotNull(taskDTO.getDateCreate());
        assertNotNull(taskDTO.getUserId());
        assertNotNull(taskDTO.getProjectId());
        assertNotNull(taskDTO.getName());
        assertNull(taskDTO.getStage());

        assertEquals(userDTO.getId(), taskDTO.getUserId());
        assertEquals(projectDTO.getId(), taskDTO.getProjectId());
    }

    @Test
    @Order(11)
    void updateTask() {

        String name = "updated name";
        String description = "updated description";
        ZonedDateTime dateBegin = ZonedDateTime.now();
        ZonedDateTime dateEnd = ZonedDateTime.now();
        Stage stage = Stage.PROCESSING;

        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskDTO.setDateBegin(dateBegin.toString());
        taskDTO.setDateEnd(dateEnd.toString());
        taskDTO.setStage(stage);

        taskDTO = taskEndpoint.updateTask(sessionDTO, taskDTO);

        assertNotNull(taskDTO);
        assertNotNull(taskDTO.getId());
        assertNotNull(taskDTO.getDateCreate());
        assertNotNull(taskDTO.getUserId());
        assertNotNull(taskDTO.getProjectId());
        assertNotNull(taskDTO.getName());
        assertNotNull(taskDTO.getDescription());
        assertNotNull(taskDTO.getDateBegin());
        assertNotNull(taskDTO.getDateEnd());
        assertNotNull(taskDTO.getStage());

        assertEquals(userDTO.getId(), taskDTO.getUserId());
        assertEquals(projectDTO.getId(), taskDTO.getProjectId());
        assertEquals(name, taskDTO.getName());
        assertEquals(description, taskDTO.getDescription());
        assertEquals(dateBegin.toString(), taskDTO.getDateBegin());
        assertEquals(dateEnd.toString(), taskDTO.getDateEnd());
        assertEquals(stage, taskDTO.getStage());
    }

    @Test
    @Order(12)
    void deleteTask() {

        taskEndpoint.deleteTask(sessionDTO, taskDTO);
        assertThrows(Exception.class, () -> taskEndpoint.readTask(sessionDTO, projectDTO.getId(), taskDTO.getId()));
    }

    @Test
    @Order(13)
    void deleteProject() {

        projectEndpoint.deleteProject(sessionDTO, projectDTO);
        assertThrows(Exception.class, () -> projectEndpoint.readProject(sessionDTO, projectDTO.getId()));
    }

    @Test
    @Order(14)
    void deleteUser() {

        userEndpoint.deleteUser(sessionDTO, userDTO);
        assertThrows(Exception.class, () -> userEndpoint.readUser(sessionDTO, sessionDTO.getUserId()));
    }

    @Test
    @Order(15)
    void terminateSession() {

        sessionEndpoint.terminateSession(sessionDTO);
    }
}
